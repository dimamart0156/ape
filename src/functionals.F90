!! Copyright (C) 2008-2014 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module functionals_m
  use global_m
  use iso_c_binding
  use messages_m
  use mesh_m
  use states_m
  use hartree_m
  use xc_f03_lib_m
  implicit none


                    !---Interfaces---!

  interface assignment (=)
    module procedure functional_copy
  end interface


                    !---Derived Data Types---!

  type functional_t
    private
    integer :: family  !< the functional family (LDA, GGA, etc.)
    integer :: id      !< functional identifier
    integer :: nspin   !< spin mode (XC_UNPOLARIZED | XC_POLARIZED)

    type(xc_f03_func_t)           :: func !< the pointer used to call the library
    type(xc_f03_func_info_t)      :: info !< information about the functional

    integer :: n_references !< number of bibliographic references
    type(xc_f03_func_reference_t) :: references(XC_MAX_REFERENCES) !< bibliographic references

    integer  :: deriv_method
    real(R8) :: dens_threshold
    integer  :: n_parameters
    real(R8), allocatable :: parameters(:)
    logical  :: fixed_tb_c
  end type functional_t


                    !---Global Variables---!

  !
  integer, parameter :: XC_DERIV_NUMERICAL  = 1, &
                        XC_DERIV_ANALYTICAL = 2

  !Some functionals not available in libxc
  integer, parameter :: XC_OEP_X_EXX  = 600, &
                        XC_OEP_XC_SIC = 601, &
                        XC_MGGA_K_GE2 = 599

  
                    !---Public/Private Statements---!

  private
  public :: functional_t, &
            functional_null, &
            functional_init, &
            assignment(=), &
            functional_get_vxc, &
            functional_get_tau, &
            functional_adsic, &
            functional_rhoxc, &
            functional_name, &
            functional_n_references, &
            functional_reference, &
            functional_doi, &
            functional_kind, &
            functional_family, &
            functional_exx_mixing, &
            functional_output_info, &
            functional_save, &
            functional_load, &
            functional_end,  &
            XC_DERIV_NUMERICAL, &
            XC_DERIV_ANALYTICAL, &
            XC_OEP_X_EXX, &
            XC_OEP_XC_SIC, &
            XC_MGGA_K_GE2

contains

  !-----------------------------------------------------------------------
  !> Nullifies and sets to zero all the components of the functional.     
  !-----------------------------------------------------------------------
  subroutine functional_null(functl)
    type(functional_t), intent(out) :: functl

    call push_sub("functional_null")

    functl%family       = 0
    functl%id           = 0
    functl%nspin        = 0
    functl%n_references = 0
    functl%deriv_method = 0
    functl%n_parameters = 0
    functl%fixed_tb_c   = .false.

    call pop_sub()
  end subroutine functional_null

  !-----------------------------------------------------------------------
  !> Initializes a functional.                                            
  !-----------------------------------------------------------------------
  subroutine functional_init(nspin, id, deriv_method, dens_threshold, n_parameters, parameters, functl)
    integer,            intent(in)    :: nspin
    integer,            intent(in)    :: id
    integer,            intent(in)    :: deriv_method
    real(R8),           intent(in)    :: dens_threshold
    integer,            intent(in)    :: n_parameters
    real(R8),           intent(in)    :: parameters(:)
    type(functional_t), intent(inout) :: functl

    call push_sub("functional_init")

    ASSERT(deriv_method == XC_DERIV_NUMERICAL .or. deriv_method == XC_DERIV_ANALYTICAL)

    functl%id = id
    functl%nspin = nspin
    functl%deriv_method = deriv_method
    functl%dens_threshold = dens_threshold
    functl%n_parameters = n_parameters
    allocate(functl%parameters(n_parameters))
    if (n_parameters > 0) then
      functl%parameters(1:n_parameters) = parameters(1:n_parameters)
    end if
      
    if(functl%id /= 0) then
      ! get the family of the functional
      if (id == XC_OEP_X_EXX) then
        functl%family = XC_FAMILY_OEP
      elseif (id == XC_MGGA_K_GE2) then
        functl%family = XC_FAMILY_MGGA
      else
        functl%family = xc_f03_family_from_id(functl%id)
      end if

      if (functl%family == XC_FAMILY_UNKNOWN) then
        write(message(1), '(a,i3,a)') "'", functl%id,"' is not a known functional!"
        message(2) = "Please check the manual for a list of possible values."
        call write_fatal(2)
      end if

    end if

    !Initialize
    if(functl%id /= 0 .and. functl%id /= XC_MGGA_K_GE2) then
      call functional_libxc_init(functl)

      if (functl%deriv_method == XC_DERIV_ANALYTICAL) then
#ifdef HAVE_LIBXC5
#ifndef HAVE_LIBXC_FXC
        if (functl%family == XC_FAMILY_GGA .or. functl%family == XC_FAMILY_MGGA) then
          write(message(1), '(a)') "APE was linked against a version of Libxc compiled without Fxc support."
          write(message(2), '(a,i3,a)') "Therefor it is not possible to compute the analytical derivatives for functional '", &
            functl%id,"'"
          call write_fatal(2)
        end if
#endif
#endif
        if ( ( (iand(xc_f03_func_info_get_flags(functl%info), XC_FLAGS_HAVE_FXC) == 0) .and. &
             (functl%family == XC_FAMILY_GGA .or. functl%family == XC_FAMILY_MGGA)   ) .or. &
             ( (iand(xc_f03_func_info_get_flags(functl%info), XC_FLAGS_HAVE_KXC) == 0) .and. &
             (functl%family == XC_FAMILY_MGGA)                                       )        ) then
          write(message(1), '(a,i3,a)') "It is not possible to compute the analytical derivatives for functional '", functl%id,"'"
          call write_fatal(1)
        end if
      end if

      if (functl%id == XC_MGGA_X_TB09) then
        ! Tran-Blaha needs to be handled in a special way, as we always need to store the C parameter.
        functl%fixed_tb_c = (functl%n_parameters == 1)
        if (functl%n_parameters == 0) then
          deallocate(functl%parameters)
          functl%n_parameters = 1
          allocate(functl%parameters(1))
        end if
      end if

    end if

    call pop_sub()
  end subroutine functional_init

  !-----------------------------------------------------------------------
  !> Initialize the libxc objects of the functional.                      
  !-----------------------------------------------------------------------
  subroutine functional_libxc_init(functl)
    type(functional_t), intent(inout) :: functl

    integer :: iref
    
    if (functl%family /= XC_FAMILY_OEP) then
      call xc_f03_func_init(functl%func, functl%id, functl%nspin)
      functl%info = xc_f03_func_get_info(functl%func)

      if (iand(xc_f03_func_info_get_flags(functl%info), XC_FLAGS_1D) /= 0) then
        write(message(1),'("1D functional ",I2, " is not supported.")') functl%id
        call write_fatal(1)
      end if
      if (iand(xc_f03_func_info_get_flags(functl%info), XC_FLAGS_2D) /= 0) then
        write(message(1),'("2D functional ",I2, " is not supported.")') functl%id
        call write_fatal(1)
      end if

      iref = 0
      functl%n_references = 0
      do       
        functl%n_references = functl%n_references + 1
        functl%references(functl%n_references) = xc_f03_func_info_get_references(functl%info, iref)
        if (iref < 0) exit
      end do

      if (functl%dens_threshold > M_ZERO) then
        call xc_f03_func_set_dens_threshold(functl%func, functl%dens_threshold)
      end if
        
      if (functl%n_parameters > 0) then
        if (xc_f03_func_info_get_n_ext_params(functl%info) /= functl%n_parameters) then
          write(message(1), '("Wrong number of external parameters in functional ",a,".")') trim(functional_name(functl))
          write(message(2), '("Functional requires ",I2," external parameters.")') xc_f03_func_info_get_n_ext_params(functl%info)
          call write_fatal(2)
        end if
        
        call xc_f03_func_set_ext_params(functl%func, functl%parameters)

      end if

    end if
      
  end subroutine functional_libxc_init

  !-----------------------------------------------------------------------
  !> Copies the functional functl_in to functl_out.                       
  !-----------------------------------------------------------------------
  subroutine functional_copy(functl_out, functl_in)
    type(functional_t), intent(inout) :: functl_out
    type(functional_t), intent(in)    :: functl_in

    call push_sub("functional_copy")

    call functional_end(functl_out)

    functl_out%family = functl_in%family
    functl_out%id     = functl_in%id
    functl_out%nspin  = functl_in%nspin

    functl_out%deriv_method = functl_in%deriv_method
    functl_out%dens_threshold = functl_in%dens_threshold
    functl_out%n_parameters = functl_in%n_parameters
    if (allocated(functl_in%parameters)) then
      allocate(functl_out%parameters(functl_out%n_parameters))
      functl_out%parameters = functl_in%parameters
    end if
    functl_out%fixed_tb_c = functl_in%fixed_tb_c

    if(functl_out%id /= 0 .and. functl_out%id /= XC_MGGA_K_GE2) then
      call functional_libxc_init(functl_out)
    end if

    call pop_sub()
  end subroutine functional_copy

  !-----------------------------------------------------------------------
  !> Frees all memory associated to the functional.                       
  !-----------------------------------------------------------------------
  subroutine functional_end(functl)
    type(functional_t), intent(inout) :: functl

    call push_sub("functional_end")

    select case (functl%family)
    case (XC_FAMILY_LDA, XC_FAMILY_GGA, XC_FAMILY_MGGA)
      if (functl%id /= XC_MGGA_K_GE2) then
        call xc_f03_func_end(functl%func)
      end if
    end select

    functl%family = 0
    functl%id     = 0
    functl%nspin  = 0

    functl%deriv_method = 0
    functl%dens_threshold = M_ZERO
    functl%n_parameters = 0
    if (allocated(functl%parameters)) then
      deallocate(functl%parameters)
    end if
    functl%fixed_tb_c = .false.
    
    call pop_sub()
  end subroutine functional_end

  !-----------------------------------------------------------------------
  !> Returns the name of the functional.                                  
  !-----------------------------------------------------------------------
  function functional_name(functl)
    type(functional_t), intent(in) :: functl
    character(128) :: functional_name

    select case (functl%id)
    case (0)
      functional_name = "None"
    case (XC_OEP_X_EXX)
      functional_name = "Exact Exchange"
    case (XC_MGGA_K_GE2)
      functional_name = "Second-order gradient expansion of the kinetic energy density"
    case default
      functional_name = xc_f03_func_info_get_name(functl%info)
    end select

  end function functional_name

  !-----------------------------------------------------------------------
  !> Returns how many references the functional has.
  !-----------------------------------------------------------------------
  function functional_n_references(functl)
    type(functional_t), intent(in) :: functl
    integer :: functional_n_references

    functional_n_references = functl%n_references

  end function functional_n_references
  
  !-----------------------------------------------------------------------
  !> Returns one of the references of the functional.                                  
  !-----------------------------------------------------------------------
  function functional_reference(functl, iref)
    type(functional_t), intent(in) :: functl
    integer,            intent(in) :: iref
    character(1024) :: functional_reference

    functional_reference = xc_f03_func_reference_get_ref(functl%references(iref))

  end function functional_reference

  !-----------------------------------------------------------------------
  !> Returns the DOI of one of the references.                                  
  !-----------------------------------------------------------------------
  function functional_doi(functl, iref)
    type(functional_t), intent(in) :: functl
    integer,            intent(in) :: iref
    character(1024) :: functional_doi

    functional_doi = xc_f03_func_reference_get_doi(functl%references(iref))

  end function functional_doi
  
  !-----------------------------------------------------------------------
  !> Returns the kind of functional we have                               
  !-----------------------------------------------------------------------
  function functional_kind(functl)
    type(functional_t), intent(in) :: functl
    integer :: functional_kind

    select case (functl%id)
    case (0)
      functional_kind = -1
    case (XC_OEP_X_EXX)
      functional_kind = XC_EXCHANGE
    case (XC_OEP_XC_SIC)
      functional_kind = XC_EXCHANGE_CORRELATION
    case (XC_MGGA_K_GE2)
      functional_kind = XC_KINETIC
    case default
      functional_kind = xc_f03_func_info_get_kind(functl%info)
    end select

  end function functional_kind

  !-----------------------------------------------------------------------
  !> Returns the family of the functional                                 
  !-----------------------------------------------------------------------
  elemental function functional_family(functl)
    type(functional_t), intent(in) :: functl
    integer :: functional_family

    functional_family = functl%family

  end function functional_family

  !-----------------------------------------------------------------------
  !> Returns the amount of exact-exchange that should be mixed with the
  !> functional. It should returnt zero if the functional is not an hybrid.
  !-----------------------------------------------------------------------
  function functional_exx_mixing(functl)
    type(functional_t), intent(in) :: functl
    real(R8) :: functional_exx_mixing

    select case (functl%family)
    case (XC_FAMILY_HYB_GGA)
      functional_exx_mixing = xc_f03_hyb_exx_coef(functl%func)
    case default
      functional_exx_mixing = M_ZERO
    end select

  end function functional_exx_mixing

  !-----------------------------------------------------------------------
  !> Writes the functional data to a file.                                
  !-----------------------------------------------------------------------
  subroutine functional_save(unit, functl)
    integer,            intent(in) :: unit
    type(functional_t), intent(in) :: functl

    integer :: ip
    
    call push_sub("functl_save")

    write(unit) functl%family
    write(unit) functl%id
    write(unit) functl%nspin

    write(unit) functl%deriv_method
    write(unit) functl%dens_threshold
    write(unit) functl%n_parameters
    if (functl%n_parameters > 0) then
      do ip = 1, functl%n_parameters
        write(unit) functl%parameters(ip)
      end do
    end if
    write(unit) functl%fixed_tb_c
    
    call pop_sub()
  end subroutine functional_save

  !-----------------------------------------------------------------------
  !> Reads the exchange-correlation model data from a file.               
  !-----------------------------------------------------------------------
  subroutine functional_load(unit, functl)
    integer,            intent(in)    :: unit
    type(functional_t), intent(inout) :: functl

    integer :: ip
    
    call push_sub("functl_load")

    read(unit) functl%family
    read(unit) functl%id
    read(unit) functl%nspin

    read(unit) functl%deriv_method
    read(unit) functl%dens_threshold
    read(unit) functl%n_parameters
    if (functl%n_parameters > 0) then
      allocate(functl%parameters(functl%n_parameters))
      do ip = 1, functl%n_parameters
        read(unit) functl%parameters(ip)
      end do
    end if
    read(unit) functl%fixed_tb_c

    if(functl%id /= 0 .and. functl%id /= XC_MGGA_K_GE2) then
      call functional_libxc_init(functl)
    end if

    call pop_sub()
  end subroutine functional_load

  !-----------------------------------------------------------------------
  !> Given a density, computes the corresponding exchange/correlation     
  !> potentials and energies.                                             
  !-----------------------------------------------------------------------
  subroutine functional_get_vxc(functl, m, rho, rho_grad, rho_lapl, tau, ip, v, e, vtau)
    type(functional_t), intent(inout) :: functl                       !< functional
    type(mesh_t),       intent(in)    :: m                            !< mesh
    real(R8),           intent(in)    :: rho(m%np, functl%nspin)      !< electronic radial density
    real(R8),           intent(in)    :: rho_grad(m%np, functl%nspin) !< gradient of the electronic radial density
    real(R8),           intent(in)    :: rho_lapl(m%np, functl%nspin) !< laplacian of the electronic radial density
    real(R8),           intent(in)    :: tau(m%np, functl%nspin)      !< radial kinetic energy density
    real(R8),           intent(in)    :: ip(functl%nspin)             !< ionization potential
    real(R8),           intent(out)   :: v(m%np, functl%nspin)        !< potential
    real(R8),           intent(out)   :: e(m%np)                      !< energy per-volume
    real(R8),           intent(out)   :: vtau(m%np, functl%nspin)     !< extra term arising from MGGA potential

#ifndef HAVE_LIBXC5
    integer :: np
#else
    integer(c_size_t) :: np
#endif
    integer  :: i, is, nspin
    real(R8) :: a, b
    real(R8), parameter   :: alpha = -0.012_r8, beta = 1.023_r8

    ! Global variables
    real(R8), allocatable :: dedrho(:,:), dedgrad(:,:), dedlapl(:,:), dedtau(:,:)
    real(R8), allocatable :: d2edrhodgrad(:,:), d2edgrad2(:,:)

    ! Local variables
    real(R8), allocatable :: n(:), s(:), l(:), t(:)
    real(R8), allocatable :: dedn(:), deds(:), dedl(:), dedt(:)
    real(R8), allocatable :: d2edn2(:), d2eds2(:), d2ednds(:)

    call push_sub("functional_get_vxc")

    ASSERT(size(v, dim=2) == functl%nspin)
    ASSERT(functional_kind(functl) /= XC_KINETIC)

    ! Initialize all output quantities to zero
    v = M_ZERO ; e = M_ZERO ; vtau = M_ZERO

    ! If the functional is not set, there is nothing to be done
    if (functl%family == 0) then
      call pop_sub()
      return
    end if

    ! Shortcut
    nspin = functl%nspin


    ! Set c parameter of the TB09 functional
    if (functl%id == XC_MGGA_X_TB09) then
      if (.not. functl%fixed_tb_c) then
        ! Compute the c parameter
        if (maxval(ip) == M_ZERO) then
          functl%parameters(1) = M_ONE
        else
          a = M_ZERO
          do
            functl%parameters(1) = alpha + beta*sqrt(M_TWO*sqrt(M_TWO*(maxval(ip) + a)))
            b = (M_THREE*functl%parameters(1) - M_TWO)/M_PI*sqrt(M_FIVE/M_SIX*(maxval(ip) + a))
            if (abs(a - b) < 1.0e-8) exit
            a = b
          end do
        end if
      end if
      call xc_f03_func_set_ext_params(functl%func, functl%parameters)
    end if


                    !---Allocate work arrays---!

    ! LDA
    allocate(n(nspin), dedn(nspin))
    allocate(dedrho(m%np, nspin))
    n = M_ZERO; dedn = M_ZERO
    dedrho = M_ZERO

    if (functl%deriv_method == XC_DERIV_ANALYTICAL) then
      if (nspin == 1) then
        allocate(d2edn2(1))
      else
        allocate(d2edn2(3))
      end if
      d2edn2 = M_ZERO
    end if

    ! GGA
    if (functl%family == XC_FAMILY_GGA .or. functl%family == XC_FAMILY_MGGA) then
      if (nspin == 1) then
        allocate(s(1), deds(1))
      else
        allocate(s(3), deds(3))
      end if
      allocate(dedgrad(m%np, nspin))
      s = M_ZERO; deds = M_ZERO
      dedgrad = M_ZERO

      if (functl%deriv_method == XC_DERIV_ANALYTICAL) then
        if (nspin == 1) then
          allocate(d2eds2(1), d2ednds(1))
          allocate(d2edgrad2(m%np, 1))
          allocate(d2edrhodgrad(m%np, 1))
        else
          allocate(d2eds2(6), d2ednds(6))
          allocate(d2edgrad2(m%np, 3))
          allocate(d2edrhodgrad(m%np, 4))
        end if
        d2eds2 = M_ZERO; d2ednds = M_ZERO
        d2edgrad2 = M_ZERO; d2edrhodgrad = M_ZERO
      end if

    end if

    ! MGGA
    if (functl%family == XC_FAMILY_MGGA) then
      allocate(l(nspin), dedl(nspin))
      allocate(t(nspin), dedt(nspin))
      allocate(dedlapl(m%np, nspin))
      allocate(dedtau(m%np, nspin))
      l = M_ZERO; dedl = M_ZERO
      t = M_ZERO; dedt = M_ZERO    
      dedlapl = M_ZERO
      dedtau = M_ZERO

      if (functl%deriv_method == XC_DERIV_ANALYTICAL) then
        !Not yet implemented
      end if

    end if


                    !---Space loop---!

    np = 1
    do i = 1, m%np
      ! make a local copy with the correct memory order
      n(1:nspin) = rho(i, 1:nspin)
      if (functl%family == XC_FAMILY_GGA .or. functl%family == XC_FAMILY_MGGA) then
        s(1) = rho_grad(i, 1)**2
        if(nspin == 2) then
          s(2) = rho_grad(i, 1)*rho_grad(i, 2)
          s(3) = rho_grad(i, 2)**2
        end if
      end if
      if (functl%family == XC_FAMILY_MGGA) then
        t(1:nspin) = tau(i, 1:nspin)/M_TWO
        l(1:nspin) = rho_lapl(i, 1:nspin)
      end if

      if (iand(xc_f03_func_info_get_flags(functl%info), XC_FLAGS_HAVE_EXC) .ne. 0) then
 
        select case(functl%family)
        case(XC_FAMILY_LDA)
          call xc_f03_lda_exc_vxc(functl%func, np, n(1), e(i), dedn(1))
        case(XC_FAMILY_GGA)
          call xc_f03_gga_exc_vxc(functl%func, np, n(1), s(1), e(i), dedn(1), deds(1))
        case(XC_FAMILY_MGGA)
          call xc_f03_mgga_exc_vxc(functl%func, np, n(1), s(1), l(1), t(1), e(i), &
                                   dedn(1), deds(1), dedl(1), dedt(1))
        end select

      else !Just get the potential

        select case(functl%family)
        case(XC_FAMILY_LDA)
          call xc_f03_lda_vxc(functl%func, np, n(1), dedn(1))
        case(XC_FAMILY_GGA)
          call xc_f03_gga_vxc(functl%func, np, n(1), s(1), dedn(1), deds(1))
        case(XC_FAMILY_MGGA)
          call xc_f03_mgga_vxc(functl%func, np, n(1), s(1), l(1), t(1), &
                               dedn(1), deds(1), dedl(1), dedt(1))
        end select
        e(i) = M_ZERO

      end if

      e(i) = e(i)*sum(n)
      dedrho(i, :) = dedn(:)
      if (functl%family == XC_FAMILY_GGA .or. functl%family == XC_FAMILY_MGGA) then
        if (nspin == 1) then
          dedgrad(i, 1) = M_TWO*deds(1)*rho_grad(i, 1)
        else
          dedgrad(i, 1) = M_TWO*deds(1)*rho_grad(i, 1) + deds(2)*rho_grad(i, 2)
          dedgrad(i, 2) = M_TWO*deds(3)*rho_grad(i, 2) + deds(2)*rho_grad(i, 1)
        end if
      end if
      if(functl%family == XC_FAMILY_MGGA) then
        dedlapl(i, 1:nspin) = dedl(1:nspin)
        dedtau(i, 1:nspin) = dedt(1:nspin)/M_TWO
      end if

      if (functl%deriv_method == XC_DERIV_ANALYTICAL) then
        !Evaluate second-order derivatives
        if (functl%family == XC_FAMILY_GGA .or. functl%family == XC_FAMILY_MGGA) then
          call xc_f03_gga_fxc(functl%func, np, n(1), s(1), d2edn2(1), d2ednds(1), d2eds2(1))

          if (nspin == 1) then
            d2edrhodgrad(i, 1) = M_TWO*rho_grad(i, 1)*d2ednds(1)
            d2edgrad2(i, 1) = M_TWO*deds(1) + M_FOUR*s(1)*d2eds2(1)
          else
            d2edrhodgrad(i, 1) = M_TWO*rho_grad(i, 1)*d2ednds(1) + rho_grad(i, 2)*d2ednds(2)
            d2edrhodgrad(i, 2) = M_TWO*rho_grad(i, 1)*d2ednds(4) + rho_grad(i, 2)*d2ednds(5)
            d2edrhodgrad(i, 3) = M_TWO*rho_grad(i, 2)*d2ednds(3) + rho_grad(i, 1)*d2ednds(2)
            d2edrhodgrad(i, 4) = M_TWO*rho_grad(i, 2)*d2ednds(6) + rho_grad(i, 1)*d2ednds(5)

            d2edgrad2(i, 1) = M_TWO*deds(1) + M_FOUR*s(1)*d2eds2(1) + M_FOUR*s(2)*d2eds2(2) + s(3)*d2eds2(4)
            d2edgrad2(i, 2) = deds(2) + M_FOUR*s(2)*d2eds2(3) + M_TWO*s(1)*d2eds2(2) + M_TWO*s(3)*d2eds2(5) + s(2)*d2eds2(4)
            d2edgrad2(i, 3) = M_TWO*deds(3) + M_FOUR*s(3)*d2eds2(6) + M_FOUR*s(2)*d2eds2(5) + s(1)*d2eds2(4)
          end if
          
        else if (functl%family == XC_FAMILY_MGGA) then
          !Not yet implemented
        end if
      end if

    end do


                    !---Compute potentials---!

    ! LDA contribution
    v = dedrho

    ! GGA contribution
    if (functl%family == XC_FAMILY_GGA .or. functl%family == XC_FAMILY_MGGA) then
      if (functl%deriv_method == XC_DERIV_NUMERICAL) then
        do is = 1, nspin
          v(:, is) = v(:, is) - mesh_divergence(m, dedgrad(:, is))
        end do
      elseif (functl%deriv_method == XC_DERIV_ANALYTICAL) then
        if (nspin == 1) then
          v(:,1) = v(:,1) - M_TWO/m%r(:)*dedgrad(:,1) &
                          - d2edgrad2(:,1)*(rho_lapl(:,1) - M_TWO/m%r(:)*rho_grad(:,1)) &
                          - d2edrhodgrad(:,1)*rho_grad(:,1)
        else         
          v(:,1) = v(:,1) - M_TWO/m%r(:)*dedgrad(:,1) &
                          - d2edgrad2(:,1)*(rho_lapl(:,1) - M_TWO/m%r(:)*rho_grad(:,1)) &
                          - d2edgrad2(:,2)*(rho_lapl(:,2) - M_TWO/m%r(:)*rho_grad(:,2)) &
                          - d2edrhodgrad(:,1)*rho_grad(:,1) &
                          - d2edrhodgrad(:,2)*rho_grad(:,2)

          v(:,2) = v(:,2) - M_TWO/m%r(:)*dedgrad(:,2) &
                          - d2edgrad2(:,3)*(rho_lapl(:,2) - M_TWO/m%r(:)*rho_grad(:,2)) &
                          - d2edgrad2(:,2)*(rho_lapl(:,1) - M_TWO/m%r(:)*rho_grad(:,1)) &
                          - d2edrhodgrad(:,3)*rho_grad(:,1) &
                          - d2edrhodgrad(:,4)*rho_grad(:,2)
        end if
      end if
    end if

    ! MGGA contribution
    if (functl%family == XC_FAMILY_MGGA) then
      if (functl%deriv_method == XC_DERIV_NUMERICAL) then
        do is = 1, nspin
          v(:, is) = v(:, is) + mesh_laplacian(m, dedlapl(:, is))
        end do
      elseif (functl%deriv_method == XC_DERIV_ANALYTICAL) then
        !Not yet implemented
      end if

      vtau = dedtau
    end if

    !Shift potentials that do not go to zero at infinity
    do is = 1, nspin
      select case (functl%id)
      case (XC_MGGA_X_BJ06)
        a = sqrt(M_FIVE/M_SIX)/M_PI
      case (XC_MGGA_X_TB09)
        a = (M_THREE*functl%parameters(1) - M_TWO)*sqrt(M_FIVE/M_SIX)/M_PI
      case (XC_GGA_X_AK13)
        a = sqrt(M_TWO)*(M_ONE/(54.0_r8*M_PI) + M_TWO/15.0_r8)
      case default
        a = M_ZERO
      end select
      do i = m%np, 1, -1
        if (v(i, is) /= M_ZERO) then
          v(1:i, is) = v(1:i, is) - a*sqrt(ip(is))
          exit
        end if
      end do
    end do

    ! If LB94, we can calculate an approximation to the energy from
    ! Levy-Perdew relation PRA 32, 2010 (1985)
    if (functl%id == XC_GGA_X_LB) then
      do is = 1, nspin
        e = e - rho(:, is)*m%r*mesh_gradient(m, v(:, is))
      end do
    end if

                    !---Deallocate arrays---!

    ! LDA
    deallocate(n, dedn, dedrho)
    if (functl%deriv_method == XC_DERIV_ANALYTICAL) then
      deallocate(d2edn2)
    end if

    ! GGA
    if (functl%family == XC_FAMILY_GGA .or. functl%family == XC_FAMILY_MGGA) then
      deallocate(s, deds, dedgrad)
      if (functl%deriv_method == XC_DERIV_ANALYTICAL) then
        deallocate(d2eds2, d2ednds, d2edgrad2, d2edrhodgrad)
      end if
    end if

    ! MGGA
    if (functl%family == XC_FAMILY_MGGA) then
      deallocate(l, dedl, dedlapl)
      deallocate(t, dedt, dedtau)
      if (functl%deriv_method == XC_DERIV_ANALYTICAL) then
        !Not yet implemented
      end if
    end if

    call pop_sub()
  end subroutine functional_get_vxc

  !-----------------------------------------------------------------------
  !> Computes the approximated kinetic energy density.                    
  !-----------------------------------------------------------------------
  subroutine functional_get_tau(functl, m, rho, rho_grad, rho_lapl, tau)
    type(functional_t), intent(in)  :: functl                       !< functional
    type(mesh_t),       intent(in)  :: m                            !< mesh
    real(R8),           intent(in)  :: rho(m%np, functl%nspin)      !< electronic radial density
    real(R8),           intent(in)  :: rho_grad(m%np, functl%nspin) !< gradient of the electronic radial density
    real(R8),           intent(in)  :: rho_lapl(m%np, functl%nspin) !< laplacian of the electronic radial density
    real(R8),           intent(out) :: tau(m%np, functl%nspin)      !< radial kinetic energy density

#ifndef HAVE_LIBXC5
    integer :: np
#else
    integer(c_size_t) :: np
#endif
    integer  :: i, is, nspin
    real(R8), allocatable :: n(:), s(:), l(:), t(:)

    call push_sub("functional_get_tau")

    ASSERT(functional_kind(functl) == XC_KINETIC)

    if (functl%id == XC_MGGA_K_GE2) then
      where (rho <= 1e-30)
        tau = M_ZERO
      elsewhere        
        tau = M_THREE/M_FIVE*(M_THREE*M_PI**2)**(M_TWO/M_THREE)*rho**(M_FIVE/M_THREE) + &
             rho_lapl/M_THREE + rho_grad**2/rho/36.0_r8
      end where
      call pop_sub()
      return
    end if

    nspin = functl%nspin

    !Allocate work arrays
    allocate(n(nspin), t(nspin))
    n = M_ZERO; t = M_ZERO
    if (functl%family == XC_FAMILY_GGA .or. functl%family == XC_FAMILY_MGGA) then
      if (nspin == 1) then
        allocate(s(1))
      else
        allocate(s(3))
      end if
      s = M_ZERO
    end if
    if (functl%family == XC_FAMILY_MGGA) then
      allocate(l(nspin))
      l = M_ZERO
    end if

    !Spin loop
    do is = 1, nspin
      !Space loop
      np = 1
      do i = 1, m%np
        ! make a local copy with the correct memory order
        n(is) = rho(i, is)

        if (functl%family == XC_FAMILY_GGA .or. functl%family == XC_FAMILY_MGGA) then
          s(1) = rho_grad(i, 1)**2
          if(nspin == 2) then
            s(2) = rho_grad(i, 1)*rho_grad(i, 2)
            s(3) = rho_grad(i, 2)**2
          end if
        end if
        if (functl%family == XC_FAMILY_MGGA) then
          l(is) = rho_lapl(i, is)
        end if

        select case(functl%family)
        case(XC_FAMILY_LDA)
          call xc_f03_lda_exc(functl%func, np, n(1), t(1))
        case(XC_FAMILY_GGA)
          call xc_f03_gga_exc(functl%func, np, n(1), s(1), t(1))
        end select

        tau(i, is) = M_TWO*t(1)*n(is)
      end do
    end do

    !Deallocate arrays
    deallocate(n, t)
    if (functl%family == XC_FAMILY_GGA .or. functl%family == XC_FAMILY_MGGA) deallocate(s)
    if (functl%family == XC_FAMILY_MGGA) deallocate(l)

    call pop_sub()
  end subroutine functional_get_tau

  !-----------------------------------------------------------------------
  !> Computes the ADSIC corrections                                       
  !-----------------------------------------------------------------------
  subroutine functional_adsic(functls, m, nspin, rho, rho_grad, rho_lapl, tau, ip, vxc, exc)
    type(functional_t), intent(inout) :: functls(2)            !< the functionals that should be corrected
    type(mesh_t),       intent(in)    :: m                     !< mesh
    integer,            intent(in)    :: nspin                 !< number of spin channels
    real(R8),           intent(in)    :: rho(m%np, nspin)      !< electronic radial density
    real(R8),           intent(in)    :: rho_grad(m%np, nspin) !< gradient of the electronic radial density
    real(R8),           intent(in)    :: rho_lapl(m%np, nspin) !< laplacian of the electronic radial density
    real(R8),           intent(in)    :: tau(m%np, nspin)      !< radial kinetic energy density
    real(R8),           intent(in)    :: ip(nspin)             !< ionization potential
    real(R8),           intent(out)   :: vxc(m%np, nspin)      !< ADSIC correction to the potential
    real(R8),           intent(out)   :: exc                   !< ADSIC correction to the energy

    integer  :: i, is
    real(R8), allocatable :: e(:), v(:,:), vtau(:,:), charge(:)
    real(R8), allocatable :: srho(:,:), sgrad(:,:), slapl(:,:), stau(:,:)

    call push_sub("functional_adsic")

    !Allocate potential and energy work arrays
    allocate(v(m%np, nspin), e(m%np), vtau(m%np, nspin), charge(nspin))
    e = M_ZERO; v = M_ZERO; vtau = M_ZERO
    exc = M_ZERO; vxc = M_ZERO
		
    !Get charge
    do is = 1, nspin
      charge(is) = M_FOUR*M_PI*mesh_integrate(m, rho(:,is))
    end do

    if (sum(charge) /= M_ZERO) then
      !Get correction for Hartree potential and energy
      call hartree_potential(m, sum(rho,dim=2), sum(charge), vh = vxc, eh = exc)
      vxc = vxc/sum(charge)
      exc = exc/sum(charge)

      allocate(srho(m%np, nspin), sgrad(m%np, nspin), slapl(m%np, nspin), stau(m%np, nspin))
      do is = 1, nspin
        if (charge(is) == M_ZERO) cycle
        srho(:, is) = rho(:, is)/charge(is)
        sgrad(:, is) = rho_grad(:, is)/charge(is)
        slapl(:, is) = rho_lapl(:, is)/charge(is)
        stau (:, is) = tau(:, is)/charge(is)
        if (nspin == 2) then
          srho (:, nspin-is+1) = M_ZERO
          sgrad(:, nspin-is+1) = M_ZERO
          slapl(:, nspin-is+1) = M_ZERO
          stau (:, nspin-is+1) = M_ZERO
        end if
        !Get xc energy and potential for the average density
        do i = 1, 2
          call functional_get_vxc(functls(i), m, srho, sgrad, slapl, stau, ip, v, e, vtau)
          exc = exc + charge(is)*M_FOUR*M_PI*mesh_integrate(m, e)
          vxc = vxc + v
        end do
      end do

      deallocate(srho, sgrad, slapl, stau)
    end if

    !Deallocates arrays
    deallocate(e, v, vtau, charge)

    call pop_sub()
  end subroutine functional_adsic

  !-----------------------------------------------------------------------
  !> Computes the rhoxc correction                                        
  !-----------------------------------------------------------------------
  subroutine functional_rhoxc(functl, m, nspin, rho, rho_grad, rho_lapl, tau, ip, dvxc)
    type(functional_t), intent(inout) :: functl                !< the functional that should be corrected
    type(mesh_t),       intent(in)    :: m                     !< mesh
    integer,            intent(in)    :: nspin                 !< number of spin channels
    real(R8),           intent(in)    :: rho(m%np, nspin)      !< electronic radial density
    real(R8),           intent(in)    :: rho_grad(m%np, nspin) !< gradient of the electronic radial density
    real(R8),           intent(in)    :: rho_lapl(m%np, nspin) !< laplacian of the electronic radial density
    real(R8),           intent(in)    :: tau(m%np, nspin)      !< radial kinetic energy density
    real(R8),           intent(in)    :: ip(nspin)             !< ionization potential
    real(R8),           intent(out)   :: dvxc(m%np, nspin)     !< correction to the potential

    integer  :: i
    real(R8) :: qxc
    real(R8), allocatable :: nxcb(:), nxc(:), vxc(:,:)
    real(R8), allocatable :: urho(:,:), ugrad(:,:), ulapl(:,:), utau(:,:)
    real(R8), allocatable :: uvxc(:,:), uexc(:), uvxctau(:,:)

    call push_sub("functional_rhoxc")

    !Get spin-unpolarized potential
    allocate(urho(m%np, nspin), ugrad(m%np, nspin), ulapl(m%np, nspin), utau(m%np, nspin))
    allocate(uvxc(m%np, nspin), uexc(m%np), uvxctau(m%np, nspin))
    do i = 1, m%np
      urho(i, :)  = sum(rho(i, 1:nspin))/nspin
      ugrad(i, :) = sum(rho_grad(i, 1:nspin))/nspin
      ulapl(i, :) = sum(rho_lapl(i, 1:nspin))/nspin
      utau(i, :)  = sum(tau(i, 1:nspin))/nspin
    end do
    call functional_get_vxc(functl, m, urho, ugrad, ulapl, utau, ip, uvxc, uexc, uvxctau)

    !Allocate xc density and potential
    allocate(nxcb(m%np), nxc(m%np), vxc(m%np, nspin))

    !Get original xc density
    nxc = -M_ONE/(M_FOUR*M_PI)*mesh_laplacian(m, uvxc)

    !Remove density from the tail such that the charge is as close as possible to -1
    nxcb = M_FOUR*M_PI*mesh_primitive(m, nxc)
    do i = m%np, 2, -1
      if ( (abs(nxcb(i-1)) > M_ONE .and. abs(nxcb(i)) <= M_ONE) .or. &
           (nxcb(i-1) - nxcb(i) > 1e-4) ) then
        nxc(i:m%np) = M_ZERO
        exit
      end if
    end do

    !Normalize xc density to -1
    qxc = M_FOUR*M_PI*mesh_integrate(m, nxc)
    if (qxc /= M_ZERO) then
      nxc = -nxc/qxc
    end if

    !Solve the Poisson equation for the xc density to get the correction to the potential
    call hartree_potential(m, nxc, -M_ONE, vh=vxc)
    dvxc = vxc - uvxc

    !Deallocates arrays
    deallocate(nxcb, nxc, vxc)
    deallocate(urho, ugrad, ulapl, utau)
    deallocate(uvxc, uexc, uvxctau)

    call pop_sub()
  end subroutine functional_rhoxc

  subroutine functional_output_info(functl, unit, verbose_limit)
    type(functional_t),           intent(in) :: functl
    integer,            optional, intent(in) :: unit
    integer,            optional, intent(in) :: verbose_limit

    integer :: n_message, iparam, iref
    real(R8) :: param
    character(1024) :: ref, doi

    call push_sub("functional_output_info")
    
    if (functl%id == 0) then
      call pop_sub()
      return
    end if
      
    select case (functional_kind(functl))
    case (XC_CORRELATION)
      write(message(1),'(2X,"Correlation:")')
    case (XC_EXCHANGE)
      write(message(1),'(2X,"Exchange:")')
    case (XC_EXCHANGE_CORRELATION)
      write(message(1),'(2X,"Exchange-Correlation:")')
    end select

    write(message(2),'(4X,"Name: ",A)') trim(functional_name(functl))

    select case (functional_family(functl))
    case (XC_FAMILY_LDA);      write(message(3),'(4X,"Family: LDA")')
    case (XC_FAMILY_GGA);      write(message(3),'(4X,"Family: GGA")')
    case (XC_FAMILY_HYB_GGA);  write(message(3),'(4X,"Family: Hybrid GGA")')
    case (XC_FAMILY_MGGA);     write(message(3),'(4X,"Family: MGGA")')
    end select

    select case (functl%deriv_method)
    case (XC_DERIV_ANALYTICAL); write(message(4),'(4X,"Derivatives: analytical")')
    case (XC_DERIV_NUMERICAL);  write(message(4),'(4X,"Derivatives: numerical")')
    end select

    if (functl%dens_threshold > M_ZERO) then
      write(message(5),'(4X,"Density threshold: ",ES8.1)') functl%dens_threshold
    else
      write(message(5),'(4X,"Density threshold: Libxc default")')
    end if

    n_message = 5
   
    do iparam = 1, xc_f03_func_info_get_n_ext_params(functl%info)
      n_message = n_message + 1
      if (functl%n_parameters > 0) then
        param = functl%parameters(iparam)
      else
        param = xc_f03_func_info_get_ext_params_default_value(functl%info, iparam-1)
      end if
      write(message(n_message),'(4X,A,": ",F15.9)') trim(xc_f03_func_info_get_ext_params_description(functl%info, iparam-1)), param
        
    end do
        
    n_message = n_message + 1
    write(message(n_message),'(4X,"References:")')
    do iref = 1, functional_n_references(functl)
      n_message = n_message + 1
      ref = functional_reference(functl, iref)
      doi = functional_doi(functl, iref)
      write(message(n_message),'(6X,"[",I1,"] ",A)') iref, ref(1:70)
      n_message = n_message + 1
      write(message(n_message),'(10X,"http://dx.doi.org/",A)') doi(1:62)
    end do
    
    call write_info(n_message, verbose_limit=verbose_limit, unit=unit)

    call pop_sub()
  end subroutine functional_output_info

end module functionals_m

