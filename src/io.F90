!! Copyright (C) 2004-2006,2011 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

module io_m
  use global_m
  use messages_m
  implicit none


                    !---Public/Private Statements---!

  private
  public :: io_open

contains

  !-----------------------------------------------------------------------
  !> Search for a non-opened unit and open a file.                        
  !-----------------------------------------------------------------------
  subroutine io_open(unit, file, status, form)
    integer,                    intent(out) :: unit   !< name of the file to open.
    character(len=*),           intent(in)  :: file   !< number of the unit.
    character(len=*), optional, intent(in)  :: status !< 
    character(len=*), optional, intent(in)  :: form   !< 

    integer :: iostat
    character(len=20) :: status_, form_

    call io_assign(unit)

    if (present(status)) then
      status_ = status
    else
      status_ = "unknown"
    end if

    if (present(form)) then
      form_ = form
    else
      form_ = "formatted"
    end if

    open(unit, file=trim(file), form=trim(form_), status=trim(status_), iostat=iostat)
    if (iostat > 0) then
      write(message(1),'("Unable to open file ''",A,"''")') trim(file)
      call write_fatal(1)
    end if

  end subroutine io_open

  !-----------------------------------------------------------------------
  !> Search for a non-opened unit between MIN_UNIT and MAX_UNIT.          
  !-----------------------------------------------------------------------
  subroutine io_assign(unit)
    integer, intent(out) :: unit !< number of the non-opened unit.

    integer, parameter :: MIN_UNIT = 10, MAX_UNIT = 100
    logical :: opened
    integer :: iostat

    do unit = MIN_UNIT, MAX_UNIT
      inquire(unit, opened = opened, iostat = iostat)
      if ((.not. opened) .and. (iostat == 0)) return
    end do
    message(1) = "No units available in io_assign"
    call write_fatal(1)
    
  end subroutine io_assign

end module io_m
