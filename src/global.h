!! Copyright (C) 2004-2006 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "config_F90.h"

#define __STRING(x)     #x

#if defined(F90_ACCEPTS_LINE_NUMBERS)
#  define ASSERT(expr)\
  if (.not.(expr)) &\newline\
\cardinal __LINE__ __FILE__ \newline\
    call assert_die(__STRING(expr), &\newline\
\cardinal __LINE__ __FILE__ \newline\
    __FILE__, &\newline\
\cardinal __LINE__ __FILE__ \newline\
    __LINE__) \newline\
\cardinal __LINE__ __FILE__ \newline
#else
#  define ASSERT(expr) \
  if(.not.(expr)) then                    \newline \
    call assert_die (__STRING(expr), &    \newline \
                      __FILE__, __LINE__)  \newline \
  end if
#endif

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
