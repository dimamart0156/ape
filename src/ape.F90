!! Copyright (C) 2004-2010 M. Oliveira, F. Nogueira
!! Copyright (C) 2016 M. Oliveira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

program ape
  use xc_f03_lib_m
  use global_m
  use oct_parser_m
  use parser_symbols_m
  use utilities_m
  use messages_m
  use run_ape_m
  implicit none

  integer :: ierr
  character(len=128) :: libxc_version
  character(len=10)  :: version = PACKAGE_VERSION
  character(len=40)  :: hash = GIT_COMMIT
  character(len=30)  :: build_time = BUILD_TIME
  character(len=66)  :: cc
  character(len=69)  :: cflags
  character(len=60)  :: fc
  character(len=63)  :: fcflags

  !Initialize parser
  ierr = oct_parse_f90_init('parser.log', 0)
  if(ierr .ne. 0) then
    message(1) = "Unable to initialize parser."
    call write_fatal(1)
  end if
  call set_parser_symbols()
  ierr = oct_parse_f90_input('inp.ape')
  if(ierr .ne. 0) then
    ierr = oct_parse_f90_input("-")
    if(ierr .ne. 0) then
      message(1) = "Error initializing the parser."
      message(2) = "Can not open input file or standard input!"
      call write_fatal(2)
    end if
  end if

  !Start messages
  call messages_init()

  !If in debug mode, create the debug directory
  if (in_debug_mode) call sys_mkdir("debug_info")

  !Print some stuff and tell the world when the program started
  message(1) = str_center("APE - Atomic Pseudopotentials Engine", 70)
  call write_info(1)
  call print_date("Program started on ")
  message(1) = "Compilation Info"
  write(message(2),'(2X,"Version:",1X,A)') version
  write(message(3),'(2X,"Commit:",1X,A)') hash
  write(message(4),'(2X,"Build time:",1X,A)') build_time
  call f_cc_string(cc)
  call f_fc_string(fc)
  call f_cflags_string(cflags)
  call f_fcflags_string(fcflags)
  write(message(5),'(2X,"C compiler:",1X,A)') trim(cc)
  write(message(6),'(2X,"C flags:",1X,A)') trim(cflags)
  write(message(7),'(2X,"Fortran compiler:",1X,A)') trim(fc)
  write(message(8),'(2X,"Fortran flags:",1X,A)') trim(fcflags)
  call xc_f03_version_string(libxc_version)
  write(message(9),'(2A)') "  Libxc version: ", trim(libxc_version)
  message(10) = ""
  call write_info(10)  

  !Now we really run the program
  call run()

  !End parser
  call oct_parse_f90_end()

  !Tell the world when the program finished
  call print_date("Program finished on ")

end program ape


