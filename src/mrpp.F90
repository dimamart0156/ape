!! Copyright (C) 2004-2011 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module multireference_m
  use global_m
  use messages_m
  use utilities_m
  use output_m
  use multiroot_m
  use mesh_m
  use quantum_numbers_m
  use potentials_m
  use wave_equations_m
  use troullier_martins_m
  use multireference_equations_m
  implicit none



                    !---Public/Private Statements---!

  private
  public :: mrpp_gen

contains

  subroutine mrpp_gen(qn0, qn1, ev0, ev1, wave_eq, rel, tol, m, ae_potential, &
                      rc, integrator, ps_v, ps_wf0, ps_wf0p, ps_wf1, ps_wf1p)
    !-----------------------------------------------------------------------!
    ! Generates the pseudo wave-functions and the corresponding             !
    ! pseudo-potential using the Multireference Pseudo-potentials scheme.   !
    !                                                                       !
    !  qn0          - quantum numbers of semi-core state                    !
    !  qn1          - quantum numbers of valence state                      !
    !  ev0          - eigenvalue of semi-core state                         !
    !  ev1          - eigenvalue of valence state                           !
    !  wave_eq      - wave equation to use                                  !
    !  rel          - if true, use the relativistic extension to the scheme !
    !  tol          - tolerance                                             !
    !  m            - mesh                                                  !
    !  ae_potential - all-electron potential                                !
    !  rc           - cutoff radius                                         !
    !  integrator   - integrator object                                     !
    !  ps_v         - pseudo-potential on the mesh                          !
    !  ps_wf0       - semi-core all-electron/pseudo wavefunction            !
    !  ps_wf0p      - semi-core all-electron/pseudo wavefunction derivative !
    !  ps_wf1       - valence all-electron/pseudo wavefunction              !
    !  ps_wf1p      - valence all-electron/pseudo wavefunction derivative   !
    !                                                                       !
    ! On input qn, ps_f and ps_g contain the all-electron quantum-numbers   !
    ! and wavefunctions and on exit the pseudo wavefunctions and quantum    !
    ! numbers.                                                              !
    !-----------------------------------------------------------------------!
    type(qn_t),         intent(inout) :: qn0          !< quantum numbers of semi-core state
    type(qn_t),         intent(inout) :: qn1          !< quantum numbers of valence state
    real(R8),           intent(in)    :: ev0          !< eigenvalue of semi-core state
    real(R8),           intent(in)    :: ev1          !< eigenvalue of valence state
    integer,            intent(in)    :: wave_eq      !< wave equation to use
    logical,            intent(in)    :: rel          !< if true, use the relativistic extension to the scheme
    real(R8),           intent(in)    :: tol          !< tolerance
    type(mesh_t),       intent(in)    :: m            !< mesh
    type(potential_t),  intent(in)    :: ae_potential !< all-electron potential
    real(R8),           intent(in)    :: rc           !< cutoff radius
    type(integrator_t), intent(inout) :: integrator   !< integrator object
    real(R8),           intent(out)   :: ps_v(m%np)   !< pseudo-potential on the mesh
    real(R8),           intent(inout) :: ps_wf0(:,:)  !< semi-core wavefunction (all-electron on input and pseudo on output)
    real(R8),           intent(inout) :: ps_wf0p(:,:) !< semi-core wavefunction derivative (all-electron on input and pseudo on output)
    real(R8),           intent(inout) :: ps_wf1(:,:)  !< valence wavefunction (all-electron on input and pseudo on output)
    real(R8),           intent(inout) :: ps_wf1p(:,:) !< valence wavefunction derivative (all-electron on input and pseudo on output)

    integer :: wf_dim, i
    real(R8) :: wf0_rc, wf0p_rc, wf1_rc, norm0
    real(R8) :: rhs(9), c(9)
    real(R8), allocatable :: ae_wf0(:,:), ae_wf0p(:,:)
    real(R8), allocatable :: ae_wf1(:,:), ae_wf1p(:,:)
    type(mesh_t) :: mrc

    call push_sub("mrpp_gen")

    !
    call mesh_null(mrc)

    !Write core radii
    write(message(1),'(4x,"Core radius:",1x,f7.3)') rc
    call write_info(1,20)
    call write_info(1,unit=info_unit("pp"))

    !Copy all-electron wavefunctions
    wf_dim = qn_wf_dim(qn0)
    allocate(ae_wf0(m%np, wf_dim), ae_wf0p(m%np, wf_dim))
    allocate(ae_wf1(m%np, wf_dim), ae_wf1p(m%np, wf_dim))
    ae_wf0 = ps_wf0
    ae_wf0p = ps_wf0p
    ae_wf1 = ps_wf1
    ae_wf1p = ps_wf1p

    !Semi-core wavefunctions and norm at rc
    wf0_rc  = rc*mesh_extrapolate(m, ae_wf0(:,1), rc)
    wf0p_rc = rc*mesh_extrapolate(m, ae_wf0p(:,1), rc) + wf0_rc/rc
    if (wf0_rc < M_ZERO) then
      ae_wf0  = -ae_wf0
      ae_wf0p = -ae_wf0p
      wf0_rc  = -wf0_rc
      wf0p_rc = -wf0p_rc
    end if
    if (rel) then
      norm0 = mesh_integrate(m, sum(ae_wf0**2,dim=2), b=rc)
    else
      select case (wave_eq)
      case (SCHRODINGER, SCALAR_REL)
        norm0 = mesh_integrate(m, ae_wf0(:,1)**2, b=rc)
      case (DIRAC)
        norm0 = mesh_integrate(m, ae_wf0(:,1)**2, b=rc) + mesh_integrate(m, ae_wf0(:,2)**2)
      end select
    end if

    !Valence wavefunctions at rc
    wf1_rc  = rc*mesh_extrapolate(m, ae_wf1(:,1), rc)

    !Get right hand side of equations to solve
    call tm_equations_rhs(m, rc, rel, qn0, ev0, wf0_rc, wf0p_rc, norm0, ae_potential, rhs(1:7))
    rhs(8) = wf1_rc
    rhs(9) = M_ZERO

    !New mesh
    mrc = m
    call mesh_truncate(mrc, rc)

    !Set the quantum numbers to their correct value
    qn0%n = qn0%l + 1
    qn1%n = qn1%l + 2

    !Solve the system of equations
    call mrpp_solve_system(m, mrc, .false., wave_eq, integrator, ae_potential,&
         rc, qn0, qn1, ev0, ev1, rhs, tol, c, ps_wf0(1:mrc%np,:), &
         ps_wf0p(1:mrc%np,:), ps_wf1(1:mrc%np,:), ps_wf1p(1:mrc%np,:), ps_v(1:mrc%np))

    !Compute the pseudo-wavefunction and the pseudopotential
    ps_wf0(mrc%np:m%np,1:wf_dim) = ae_wf0(mrc%np:m%np,1:wf_dim)
    ps_wf0p(mrc%np:m%np,1:wf_dim) = ae_wf0p(mrc%np:m%np,1:wf_dim)
    ps_wf1(mrc%np:m%np,1:wf_dim) = ae_wf1(mrc%np:m%np,1:wf_dim)
    ps_wf1p(mrc%np:m%np,1:wf_dim) = ae_wf1p(mrc%np:m%np,1:wf_dim)
    if (wave_eq == DIRAC .and. .not. rel) then
      ps_wf0(mrc%np:m%np,2) = M_ZERO
      ps_wf0p(mrc%np:m%np,2) = M_ZERO
      ps_wf1(mrc%np:m%np,2) = M_ZERO
      ps_wf1p(mrc%np:m%np,2) = M_ZERO
    end if
    do i = mrc%np, m%np
      ps_v(i) = v(ae_potential, m%r(i), qn0)
    end do

    !Free memory
    deallocate(ae_wf0, ae_wf0p, ae_wf1, ae_wf1p)
    call mesh_end(mrc)

    call pop_sub()
  end subroutine mrpp_gen

  !-----------------------------------------------------------------------
  !> Solve the MRPP set of non-linear equations.                          
  !-----------------------------------------------------------------------
  subroutine mrpp_solve_system(m, mrc, rel, wave_eq, integrator, ae_potential,&
                               rc, qn0, qn1, ev0, ev1, rhs, tol, c, &
                               ps_wf0, ps_wf0p, ps_wf1, ps_wf1p, ps_v)
    type(mesh_t),       intent(in)    :: m            !< mesh
    type(mesh_t),       intent(in)    :: mrc          !< truncated mesh
    logical,            intent(in)    :: rel          !< if true, use the relativistic extension to the scheme
    integer,            intent(in)    :: wave_eq      !< wave equation to use
    type(integrator_t), intent(inout) :: integrator   !< integrator object
    type(potential_t),  intent(in)    :: ae_potential !< all-electron potential
    real(R8),           intent(in)    :: rc           !< cutoff radius
    type(qn_t),         intent(in)    :: qn0          !< quantum numbers of semi-core state
    type(qn_t),         intent(in)    :: qn1          !< quantum numbers of valence state
    real(R8),           intent(in)    :: ev0          !< eigenvalue of semi-core state
    real(R8),           intent(in)    :: ev1          !< eigenvalue of valence state
    real(R8),           intent(in)    :: rhs(9)       !< right-hand side of equations
    real(R8),           intent(in)    :: tol          !< tolerance
    real(R8),           intent(out)   :: c(9)         !< coeficients of the polynomial
    real(R8),           intent(inout) :: ps_wf0(:,:)  !< semi-core state pseudo wavefunction
    real(R8),           intent(inout) :: ps_wf0p(:,:) !< semi-core state pseudo wavefunction derivatives
    real(R8),           intent(inout) :: ps_wf1(:,:)  !< valence state pseudo wavefunction
    real(R8),           intent(inout) :: ps_wf1p(:,:) !< valence state pseudo wavefunction derivatives
    real(R8),           intent(out)   :: ps_v(mrc%np) !< pseudo-potential

    real(R8) :: ldd, cc(4), f(4)
    type(multiroot_solver_t) :: mr_solver

    call push_sub("mrpp_solve_system")

    !Solve TM system: we will use it as a starting point
    c = M_ZERO
    call tm_solve_system(mrc, rc, rel, qn0, ev0, rhs(1:7), tol, c(1:7), ps_wf0, ps_wf0p, ps_v, .true.)

    !Allocate memory
    call mrpp_equations_init(m, mrc, rel, wave_eq, integrator, ae_potential, rc, qn0, qn1, ev0, ev1, rhs)

    !Solve system
    cc(1) = c(1)
    cc(2:4) = c(7:9)
    call multiroot_solver_init(4, M_ZERO, tol, M_ZERO, 300, BROYDEN, mr_solver)
    call multiroot_solve_system(mr_solver, cc, mrpp_equations, mrpp_equations_write_info)

    !Get the final coefficients
    c(1) = cc(1)
    c(7:9) = cc(2:4)
    call mrpp_solve_linear_system(c)

    !Compute the final pseudo-wavefunction and the pseudopotential
    call mrpp_ps_wavefunctions(c, ps_wf0, ps_wf0p, ps_v, ldd, ps_wf1, ps_wf1p)

    !Write final results
    call mrpp_equations(4, cc, f)
    write(message(1), '(4x,"Coefficients:")')
    write(message(2), '(6x,"c0  =",1x,es16.9e2,3x,"c2  =",1x,es16.9e2)') c(1), c(2)
    write(message(3), '(6x,"c4  =",1x,es16.9e2,3x,"c6  =",1x,es16.9e2)') c(3), c(4)
    write(message(4), '(6x,"c8  =",1x,es16.9e2,3x,"c10 =",1x,es16.9e2)') c(5), c(6)
    write(message(5), '(6x,"c12 =",1x,es16.9e2,3x,"c14 =",1x,es16.9e2)') c(7), c(8)
    write(message(6), '(6x,"c16 =",1x,es16.9e2)') c(9)
    write(message(7), '(4x,"Residues:")')
    write(message(8), '(6X,"c2**2 + c4(2l+5)                  =",1x,ES16.9e2)') f(1)
    write(message(9), '(6X,"norm_sc[AE] - norm_sc[PP]         =",1x,ES16.9e2)') f(2)
    write(message(10),'(6X,"R(rc)[AE] - R(rc)[PP]             =",1x,ES16.9e2)') f(3)
    write(message(11),'(6X,"R''/R(rc)[AE] - R''/R(rc)[PP]       =",1x,ES16.9e2)') f(4)
    message(12) = ""
    call write_info(12,20)
    call write_info(12,unit=info_unit("pp"))

    !Free memory
    call mrpp_equations_end()

    call pop_sub()
  end subroutine mrpp_solve_system

end module multireference_m
