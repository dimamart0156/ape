!! Copyright (C) 2004-2010 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module global_m
  implicit none

                    !---Names of kind types parameters---!

  !8 bytes real
  integer, parameter :: R8 = 8

                    !---Parameters---!

  real(R8), parameter :: M_ZERO     = 0.0_r8
  real(R8), parameter :: M_ONE      = 1.0_r8
  real(R8), parameter :: M_TWO      = 2.0_r8
  real(R8), parameter :: M_THREE    = 3.0_r8
  real(R8), parameter :: M_FOUR     = 4.0_r8
  real(R8), parameter :: M_FIVE     = 5.0_r8
  real(R8), parameter :: M_SIX      = 6.0_r8
  real(R8), parameter :: M_SEVEN    = 7.0_r8
  real(R8), parameter :: M_EIGHT    = 8.0_r8
  real(R8), parameter :: M_NINE     = 9.0_r8
  real(R8), parameter :: M_TEN      = 10.0_r8
  real(R8), parameter :: M_ELEVEN   = 11.0_r8
  real(R8), parameter :: M_TWELVE   = 12.0_r8
  real(R8), parameter :: M_FOURTEEN = 14.0_r8
  real(R8), parameter :: M_SIXTEEN  = 16.0_r8
  real(R8), parameter :: M_EIGHTEEN = 18.0_r8
  real(R8), parameter :: M_TWENTY   = 20.0_r8
  real(R8), parameter :: M_THIRTY   = 30.0_r8
  real(R8), parameter :: M_FIFTY    = 50.0_r8
  real(R8), parameter :: M_SIXTY    = 60.0_r8
  real(R8), parameter :: M_HUNDRED  = 100.0_r8
  real(R8), parameter :: M_HALF     = 0.5_r8
  real(R8), parameter :: M_THIRD    = M_ONE/M_THREE
  real(R8), parameter :: M_TWOTHIRD = M_TWO/M_THREE
  real(R8), parameter :: M_DIME     = 0.1_r8
  real(R8), parameter :: M_CENT     = 0.01_r8
  real(R8), parameter :: M_PI       = 3.141592653589793238462643383279502884197_r8
  real(R8), parameter :: M_C        = 137.03599976_r8
  real(R8), parameter :: M_C2       = 18778.86524_r8
  real(R8), parameter :: M_LOGC     = 4.92024366328042038029011278105_r8
  real(R8), parameter :: M_EPSILON  = 1.0e-20_r8

  !Other stuff
  logical :: in_debug_mode = .false. !< If true, execute debug instructions.

end module global_m
