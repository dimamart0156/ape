!! Copyright (C) 2014 P. Borlido, M. Oliveira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!


#include "global.h"

module hf_potentials_m
  use global_m
  use messages_m
  use io_m
  use units_m
  use splines_m
  use mesh_m
  use quantum_numbers_m
  use ps_io_m
  implicit none


                    !---Interfaces---!

  interface assignment (=)
     module procedure hf_potential_copy
  end interface


                    !---Derived Data Types---!

  type hf_potential_t
!    private

    integer :: nc ! number of components (states)
    type(qn_t),     pointer :: qn(:)

    !potential for each qn
    real(R8),       pointer :: v(:,:) 
    type(spline_t), pointer :: v_spl(:)
    type(spline_t), pointer :: vp_spl(:)
    type(spline_t), pointer :: vpp_spl(:)

    !non homogeneous part
    real(R8),       pointer :: n(:,:)
    type(spline_t), pointer :: n_spl(:)
    type(spline_t), pointer :: np_spl(:)
    type(spline_t), pointer :: npp_spl(:)
  end type hf_potential_t


                    !---Public/Private Statements---!

  private
  public :: hf_potential_t, &
            hf_potential_null, &
            hf_potential_init, &
            assignment(=), &
            hf_potential_update, &
            hf_potential_end, &
            hf_potential_save, &
            hf_potential_load, &
            hf_v, &
            hf_dvdr, &
            hf_d2vdr2, &
            hf_n,&
            hf_potential_copy,&
            hf_potential_output


contains

  !-----------------------------------------------------------------------
  !> Nullifies and sets to zero all the components of the potential.      
  !-----------------------------------------------------------------------
  subroutine hf_potential_null(potential)
    type(hf_potential_t), intent(out) :: potential

    call push_sub("hf_potential_null")

    potential%nc = 0
    nullify(potential%qn)
    nullify(potential%v)
    nullify(potential%v_spl)
    nullify(potential%vp_spl)
    nullify(potential%vpp_spl)
    nullify(potential%n)
    nullify(potential%n_spl)
    nullify(potential%np_spl)
    nullify(potential%npp_spl)

    call pop_sub()
  end subroutine hf_potential_null

  !-----------------------------------------------------------------------
  !> 
  !-----------------------------------------------------------------------
  subroutine hf_potential_init(potential, m, nc, qn, v, n)
    type(hf_potential_t), intent(inout) :: potential
    type(mesh_t),         intent(in)    :: m
    integer,              intent(in)    :: nc 
    type(qn_t),           intent(in)    :: qn(nc)
    real(R8),             intent(in)    :: v(m%np, nc)
    real(R8),             intent(in)    :: n(m%np, nc)

    integer :: ii

    call push_sub("hf_potential_init")

    potential%nc = nc

    allocate(potential%v(m%np, nc))
    allocate(potential%n(m%np, nc))
    allocate(potential%qn(nc))
    allocate(potential%v_spl(nc))
    allocate(potential%vp_spl(nc))
    allocate(potential%vpp_spl(nc))
    allocate(potential%n_spl(nc))
    allocate(potential%np_spl(nc))
    allocate(potential%npp_spl(nc))

    do ii = 1, nc
      potential%qn(ii)  = qn(ii)
      potential%v(:,ii) = v(:,ii)
      potential%n(:,ii) = n(:,ii)

      call spline_null(potential%v_spl(ii))
      call spline_null(potential%vp_spl(ii))
      call spline_null(potential%vpp_spl(ii))
      call spline_init(potential%v_spl(ii), m%np, m%r ,potential%v(:,ii), 3)
      call spline_init(potential%vp_spl(ii), m%np, m%r, mesh_derivative(m, potential%v(:,ii)), 3)
      call spline_init(potential%vpp_spl(ii), m%np, m%r, mesh_derivative2(m, potential%v(:,ii)), 3)
 
      call spline_null(potential%n_spl(ii))
      call spline_null(potential%np_spl(ii))
      call spline_null(potential%npp_spl(ii))
      call spline_init(potential%n_spl(ii), m%np, m%r ,potential%n(:,ii), 3)
      call spline_init(potential%np_spl(ii), m%np, m%r, mesh_derivative(m, potential%n(:,ii)), 3)
      call spline_init(potential%npp_spl(ii), m%np, m%r, mesh_derivative2(m, potential%n(:,ii)), 3)
    end do

    call pop_sub()
  end subroutine hf_potential_init

  !-----------------------------------------------------------------------
  !> Copies the potential potential_a to potential potential_b.           
  !-----------------------------------------------------------------------
  subroutine hf_potential_copy(potential_a, potential_b)
    type(hf_potential_t), intent(inout) :: potential_a
    type(hf_potential_t), intent(in)    :: potential_b

    integer :: i

    call push_sub("hf_potential_copy")

    call hf_potential_end(potential_a)

    potential_a%nc = potential_b%nc

    allocate(potential_a%qn(potential_a%nc))
    potential_a%qn = potential_b%qn

    allocate(potential_a%v(size(potential_b%v, dim=1), potential_a%nc))
    allocate(potential_a%n(size(potential_b%n, dim=1), potential_a%nc))
    potential_a%v = potential_b%v
    potential_a%n = potential_b%n
    allocate(potential_a%v_spl(potential_a%nc))
    allocate(potential_a%vp_spl(potential_a%nc))
    allocate(potential_a%vpp_spl(potential_a%nc))
    allocate(potential_a%n_spl(potential_a%nc))
    allocate(potential_a%np_spl(potential_a%nc))
    allocate(potential_a%npp_spl(potential_a%nc))
    do i = 1, potential_a%nc
      call spline_null(potential_a%v_spl(i))
      call spline_null(potential_a%vp_spl(i))
      call spline_null(potential_a%vpp_spl(i))
      call spline_null(potential_a%n_spl(i))
      call spline_null(potential_a%np_spl(i))
      call spline_null(potential_a%npp_spl(i))
      potential_a%v_spl(i) = potential_b%v_spl(i)
      potential_a%vp_spl(i) = potential_b%vp_spl(i)
      potential_a%vpp_spl(i) = potential_b%vpp_spl(i)
      potential_a%n_spl(i) = potential_b%n_spl(i)
      potential_a%np_spl(i) = potential_b%np_spl(i)
      potential_a%npp_spl(i) = potential_b%npp_spl(i)
    end do

    call pop_sub()
  end subroutine hf_potential_copy

  !-----------------------------------------------------------------------!
  !>
  !-----------------------------------------------------------------------!
  subroutine hf_potential_update(potential, m, nc, v, n)
    type(hf_potential_t), intent(inout) :: potential
    type(mesh_t),         intent(in)    :: m
    integer,              intent(in)    :: nc
    real(R8),             intent(in)    :: v(m%np, nc)
    real(R8),             intent(in)    :: n(m%np, nc)

    integer :: i

    call push_sub("hf_potential_update")

    ASSERT(nc == potential%nc)

    potential%v = v
    potential%n = n
    do i = 1, potential%nc
      call spline_end(potential%v_spl(i))
      call spline_end(potential%vp_spl(i))
      call spline_end(potential%vpp_spl(i))
      call spline_init(potential%v_spl(i), m%np, m%r ,potential%v(:,i), 3)
      call spline_init(potential%vp_spl(i), m%np, m%r, mesh_derivative(m, potential%v(:,i)), 3)
      call spline_init(potential%vpp_spl(i), m%np, m%r, mesh_derivative2(m, potential%v(:,i)), 3)

      call spline_end(potential%n_spl(i))
      call spline_end(potential%np_spl(i))
      call spline_end(potential%npp_spl(i))
      call spline_init(potential%n_spl(i), m%np, m%r ,potential%n(:,i), 3)
      call spline_init(potential%np_spl(i), m%np, m%r, mesh_derivative(m, potential%n(:,i)), 3)
      call spline_init(potential%npp_spl(i), m%np, m%r, mesh_derivative2(m, potential%n(:,i)), 3)
    end do

    call pop_sub()
  end subroutine hf_potential_update

  !-----------------------------------------------------------------------!
  !>
  !-----------------------------------------------------------------------!
  subroutine hf_potential_end(potential)
    type(hf_potential_t), intent(inout) :: potential

    integer :: i

    call push_sub("hf_potential_end")

    if (associated(potential%qn)) then
      deallocate (potential%qn)
    end if
    if (associated(potential%v)) then
      deallocate (potential%v)
    end if
    if (associated(potential%v_spl)) then
      do i = 1, potential%nc
        call spline_end(potential%v_spl(i))
      end do
      deallocate(potential%v_spl)
    end if
    if (associated(potential%vp_spl)) then
      do i = 1, potential%nc
        call spline_end(potential%vp_spl(i))
      end do
      deallocate(potential%vp_spl)
    end if
    if (associated(potential%vpp_spl)) then
      do i = 1, potential%nc
        call spline_end(potential%vpp_spl(i))
      end do
      deallocate(potential%vpp_spl)
    end if
    if (associated(potential%n)) then
      deallocate (potential%n)
    end if
    if (associated(potential%n_spl)) then
      do i = 1, potential%nc
        call spline_end(potential%n_spl(i))
      end do
      deallocate(potential%n_spl)
    end if
    if (associated(potential%np_spl)) then
      do i = 1, potential%nc
        call spline_end(potential%np_spl(i))
      end do
      deallocate(potential%np_spl)
    end if
    if (associated(potential%npp_spl)) then
      do i = 1, potential%nc
        call spline_end(potential%npp_spl(i))
      end do
      deallocate(potential%npp_spl)
    end if

    potential%nc = 0

    call pop_sub()
  end subroutine hf_potential_end

  !-----------------------------------------------------------------------
  !> Writes the potential to a file.                                      
  !-----------------------------------------------------------------------
  subroutine hf_potential_save(unit, m, potential)
    integer,              intent(in) :: unit      !< file unit number
    type(mesh_t),         intent(in) :: m         !< the mesh
    type(hf_potential_t), intent(in) :: potential !< potential to be written

    integer :: ic, ip

    call push_sub("hf_potential_save")

    write(unit) potential%nc

    do ic = 1, potential%nc
      write(unit) potential%qn(ic)
      do ip = 1, m%np
        write(unit) potential%v(ip, ic)
      end do
      do ip = 1, m%np
        write(unit) potential%n(ip, ic)
      end do
    end do

    call pop_sub()
  end subroutine hf_potential_save

  !-----------------------------------------------------------------------
  !> Reads the potential from a file.                                     
  !-----------------------------------------------------------------------
  subroutine hf_potential_load(unit, m, potential)
    integer,              intent(in)    :: unit      !< file unit number
    type(mesh_t),         intent(in)    :: m         !< the mesh
    type(hf_potential_t), intent(inout) :: potential !< potential to be read

    integer :: ip, ic

    call push_sub("hf_potential_load")

    read(unit) potential%nc

    allocate(potential%v(m%np, potential%nc))
    allocate(potential%n(m%np, potential%nc))
    allocate(potential%qn(potential%nc))
    allocate(potential%v_spl(potential%nc))
    allocate(potential%vp_spl(potential%nc))
    allocate(potential%vpp_spl(potential%nc))
    allocate(potential%n_spl(potential%nc))
    allocate(potential%np_spl(potential%nc))
    allocate(potential%npp_spl(potential%nc))

    do ic = 1, potential%nc
      read(unit) potential%qn(ic)

      do ip = 1, m%np
        read(unit) potential%v(ip, ic)
      end do
      call spline_null(potential%v_spl(ic))
      call spline_null(potential%vp_spl(ic))
      call spline_null(potential%vpp_spl(ic))
      call spline_init(potential%v_spl(ic), m%np, m%r ,potential%v(:,ic), 3)
      call spline_init(potential%vp_spl(ic), m%np, m%r, mesh_derivative(m, potential%v(:,ic)), 3)
      call spline_init(potential%vpp_spl(ic), m%np, m%r, mesh_derivative2(m, potential%v(:,ic)), 3)

      do ip = 1, m%np
        read(unit) potential%n(ip, ic)
      end do
      call spline_null(potential%n_spl(ic))
      call spline_null(potential%np_spl(ic))
      call spline_null(potential%npp_spl(ic))
      call spline_init(potential%n_spl(ic), m%np, m%r ,potential%n(:,ic), 3)
      call spline_init(potential%np_spl(ic), m%np, m%r, mesh_derivative(m, potential%n(:,ic)), 3)
      call spline_init(potential%npp_spl(ic), m%np, m%r, mesh_derivative2(m, potential%n(:,ic)), 3)
    end do

    call pop_sub()
  end subroutine hf_potential_load

  !-----------------------------------------------------------------------
  !> Returns the potential for electron with qn at point r
  !-----------------------------------------------------------------------
  function hf_v(potential, r, qn)
    type(hf_potential_t), intent(in) :: potential
    real(R8),             intent(in) :: r
    type(qn_t),           intent(in) :: qn
    real(R8) :: hf_v

    integer :: ii

    do ii = 1, potential%nc
      if (qn == potential%qn(ii)) exit
    end do

    hf_v = spline_eval(potential%v_spl(ii), r)

  end function hf_v

  !-----------------------------------------------------------------------
  !>
  !-----------------------------------------------------------------------
  function hf_dvdr(potential, r, qn)
    type(hf_potential_t), intent(in) :: potential
    real(R8),             intent(in) :: r
    type(qn_t),           intent(in) :: qn
    real(R8) :: hf_dvdr

    integer :: ii

    do ii = 1, potential%nc
      if (qn == potential%qn(ii)) exit
    end do

    hf_dvdr = spline_eval(potential%vp_spl(ii), r)

  end function hf_dvdr

  !-----------------------------------------------------------------------
  !>
  !-----------------------------------------------------------------------
  function hf_d2vdr2(potential, r, qn)
    type(hf_potential_t), intent(in) :: potential
    real(R8),             intent(in) :: r
    type(qn_t),           intent(in) :: qn
    real(R8) :: hf_d2vdr2

    integer :: ii

    do ii = 1, potential%nc
      if (qn == potential%qn(ii)) exit
    end do

    hf_d2vdr2 = spline_eval(potential%vpp_spl(ii), r)

  end function hf_d2vdr2

  !-----------------------------------------------------------------------
  !> Returns the non homogeneous part for electron with qn at point r
  !-----------------------------------------------------------------------
  function hf_n(potential, r, qn)
    type(hf_potential_t), intent(in) :: potential
    real(R8),             intent(in) :: r
    type(qn_t),           intent(in) :: qn
    real(R8) :: hf_n

    integer :: ii

    do ii = 1, potential%nc
      if (qn == potential%qn(ii)) exit
    end do

    hf_n = spline_eval(potential%n_spl(ii), r)

  end function hf_n

  !-----------------------------------------------------------------------
  !>
  !-----------------------------------------------------------------------
  function hf_dndr(potential, r, qn)
    type(hf_potential_t), intent(in) :: potential
    real(R8),             intent(in) :: r
    type(qn_t),           intent(in) :: qn
    real(R8) :: hf_dndr

    integer :: ii

    do ii = 1, potential%nc
      if (qn == potential%qn(ii)) exit
    end do

    hf_dndr = spline_eval(potential%np_spl(ii), r)

  end function hf_dndr

  !-----------------------------------------------------------------------
  !>
  !-----------------------------------------------------------------------
  function hf_d2ndr2(potential, r, qn)
    type(hf_potential_t), intent(in) :: potential
    real(R8),             intent(in) :: r
    type(qn_t),           intent(in) :: qn
    real(R8) :: hf_d2ndr2

    integer :: ii

    do ii=1, potential%nc
      if (qn==potential%qn(ii)) exit
    end do

    hf_d2ndr2 = spline_eval(potential%npp_spl(ii), r)

  end function hf_d2ndr2

  !-----------------------------------------------------------------------
  !>
  !-----------------------------------------------------------------------
  subroutine hf_potential_debug(potential)
    type(hf_potential_t), intent(in) :: potential

    call push_sub("hf_potential_debug")

    call pop_sub()
  end subroutine hf_potential_debug

  !-----------------------------------------------------------------------
  !>
  !-----------------------------------------------------------------------
  subroutine hf_potential_output(potential, m, dir)
    type(hf_potential_t), intent(in) :: potential
    type(mesh_t),         intent(in) :: m
    character(len=*),     intent(in) :: dir

    integer  :: i, k, unit
    real(R8) :: ue, ul
    character(len=10) :: label
    character(len=20) :: filename

    call push_sub("hf_potential_output")

    ul = units_out%length%factor
    ue = units_out%energy%factor

    do k = 1, potential%nc
      label = qn_label(potential%qn(k))
      filename = trim(dir)//"/v_hf-"//trim(label)
      call io_open(unit, file=trim(filename))

      write(unit,'("# ")')
      write(unit,'("# Energy units: ",A)') trim(units_out%energy%name)
      write(unit,'("# Length units: ",A)') trim(units_out%length%name)
      write(unit,'("#")')
      write(unit,'("# ",53("-"))')
      write(unit,'("# |",7X,"r",7X,"|",6X,"v(r)",7X,"|",6X,"x(r)",7X,"|")')
      write(unit,'("# ",53("-"))')
      do i = 1, m%np
        write(unit,'(3X,ES14.8E2,2(3X,ES15.8E2))') m%r(i)/ul, &
             potential%v(i, k)/ue, potential%n(i, k)/ue
      end do
      close(unit)
    end do

    call pop_sub()
  end subroutine hf_potential_output

end module hf_potentials_m
