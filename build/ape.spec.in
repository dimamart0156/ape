## Copyright (C) 2008-2010 M. Oliveira
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##

# RPM spec file for @PACKAGE@.
# This file is used to build Redhat Package Manager packages for the
# @PACKAGE@.  Such packages make it easy to install and uninstall
# the library and related files from binaries or source.
#
# This spec file is for version @VERSION@ of @PACKAGE@; the appropriate
# version numbers are automatically substituted in to @PACKAGE@.spec.in
# by the configure script.  However, @PACKAGE@.spec.in may need to be
# modified for future releases, if the list of installed files
# or build commands change.
#
# RPM.  To build, use the command: rpm --clean -ba @PACKAGE@.spec
#
# Alternatively, you can just use 'make rpm'.
#
Name: @PACKAGE@
Summary: atomic DFT code, pseudopotential generator
Version: @VERSION@
Release: 1
License: GPL 2.0
Group: Applications/Scientific
BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
Source: http://www.tddft.org/programs/APE/download/%{name}-%{version}.tar.gz
URL: http://www.tddft.org/programs/APE
Requires(post): info
Requires(preun): info

%description
APE is a computer package designed to generate and test
norm-conserving pseudopotentials within Density Functional
Theory. The generated pseudo-potentials can be either non-
relativistic, scalar relativistic or fully relativistic and can
explicitly include semi-core states.  A wide range of
exchange-correlation functionals is included.

%prep
%setup -q

%build
%configure \
  FC="@FC@" \
  FCFLAGS="@FCFLAGS@" \
  CFLAGS="@CFLAGS@" \
  CPPFLAGS="@CPPFLAGS@" \
  LDFLAGS="@LDFLAGS@" \
  GSL_CFLAGS="@GSL_CFLAGS@" \
  GSL_CONFIG="@GSL_CONFIG@" \
  GSL_LIBS="@GSL_LIBS@"
make
(cd doc; make ape.pdf)

%install
rm -rf ${RPM_BUILD_ROOT}
make install DESTDIR=${RPM_BUILD_ROOT}
rm -f $RPM_BUILD_ROOT/usr/share/info/dir

%clean
rm -rf ${RPM_BUILD_ROOT}

%post
/sbin/install-info %{_infodir}/ape.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ] ; then
/sbin/install-info --delete %{_infodir}/ape.info.gz %{_infodir}/dir || :
fi

%files
%defattr(-,root,root,0755)
%doc README NEWS COPYING AUTHORS doc/ape.pdf
%{_bindir}/*
%{_datadir}/ape/*
%{_infodir}/ape.info.gz
