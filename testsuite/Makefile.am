## Process this file with automake to produce Makefile.in

## Copyright (C) 2005-2011 M. Oliveira, F. Nogueira
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##

# This testsuite is based on the one of the Octopus code (www.tddft.org/programs/octopus)

SUBDIRS = basic all_electron pseudopotentials libxc

bin_SCRIPTS = ape-run_regression_test.pl ape-run_testsuite

EXTRA_DIST = ape-run_regression_test.pl ape-run_testsuite.in


# If the testsuite should be skipped, e. g. in make distcheck,
# set the environment variable SKIP_CHECK to some value.
check:
	@if test "x$(SKIP_CHECK)" == "x"; then \
	    base=`basename "$(top_builddir)/share"` && \
	    dir=`dirname "$(top_builddir)/share"` && \
	    sharedir="`(cd \"$$dir\" 2> /dev/null && pwd || echo \"$$dir\")`/$$base" && \
	    APE_SHARE=$$sharedir ./ape-run_testsuite -c -d $(srcdir) -l -g all; \
	else \
	    echo "Skipping checks"; \
	fi

check-basic:
	@if test "x$(SKIP_CHECK)" == "x"; then \
	    base=`basename "$(top_builddir)/share"` && \
	    dir=`dirname "$(top_builddir)/share"` && \
	    sharedir="`(cd \"$$dir\" 2> /dev/null && pwd || echo \"$$dir\")`/$$base" && \
	    APE_SHARE=$$sharedir ./ape-run_testsuite -c -d $(srcdir) -l -g basic; \
	else \
	    echo "Skipping checks"; \
	fi

check-ae:
	@if test "x$(SKIP_CHECK)" == "x"; then \
	    base=`basename "$(top_builddir)/share"` && \
	    dir=`dirname "$(top_builddir)/share"` && \
	    sharedir="`(cd \"$$dir\" 2> /dev/null && pwd || echo \"$$dir\")`/$$base" && \
	    APE_SHARE=$$sharedir ./ape-run_testsuite -c -d $(srcdir) -l -g ae; \
	else \
	    echo "Skipping checks"; \
	fi

check-ps:
	@if test "x$(SKIP_CHECK)" == "x"; then \
	    base=`basename "$(top_builddir)/share"` && \
	    dir=`dirname "$(top_builddir)/share"` && \
	    sharedir="`(cd \"$$dir\" 2> /dev/null && pwd || echo \"$$dir\")`/$$base" && \
	    APE_SHARE=$$sharedir ./ape-run_testsuite -c -d $(srcdir) -l -g ps; \
	else \
	    echo "Skipping checks"; \
	fi

check-libxc:
	@if test "x$(SKIP_CHECK)" == "x"; then \
	    base=`basename "$(top_builddir)/share"` && \
	    dir=`dirname "$(top_builddir)/share"` && \
	    sharedir="`(cd \"$$dir\" 2> /dev/null && pwd || echo \"$$dir\")`/$$base" && \
	    APE_SHARE=$$sharedir ./ape-run_testsuite -c -d $(srcdir) -l -g libxc; \
	else \
	    echo "Skipping checks"; \
	fi

check-update-ref:
	@if test "x$(SKIP_CHECK)" == "x"; then \
	    base=`basename "$(top_builddir)/share"` && \
	    dir=`dirname "$(top_builddir)/share"` && \
	    sharedir="`(cd \"$$dir\" 2> /dev/null && pwd || echo \"$$dir\")`/$$base" && \
	    APE_SHARE=$$sharedir ./ape-run_testsuite -c -d $(srcdir) -l -u -g all; \
	else \
	    echo "Skipping checks"; \
	fi

check-update-tol:
	@if test "x$(SKIP_CHECK)" == "x"; then \
	    base=`basename "$(top_builddir)/share"` && \
	    dir=`dirname "$(top_builddir)/share"` && \
	    sharedir="`(cd \"$$dir\" 2> /dev/null && pwd || echo \"$$dir\")`/$$base" && \
	    APE_SHARE=$$sharedir ./ape-run_testsuite -c -d $(srcdir) -l -t -g all; \
	else \
	    echo "Skipping checks"; \
	fi

clean-local:
	-find /tmp -name 'ape*' -type d -mmin +600 -exec rm -rf {} \; 2>/dev/null

.PHONY: check check-basic check-ae check-ps check-libxc clean clean-local
