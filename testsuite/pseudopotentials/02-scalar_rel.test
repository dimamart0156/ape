
Test     : Pseudopotentials: scalar-relativistic Cr
Programs : ape
TestGroups : all; ps
Enabled  : Yes

Input: 02-scalar_rel.01-ae.inp

Input: 02-scalar_rel.02-hamann.inp

#Coefficient
match; 4s cl coefficient ; GREP(pp/info, 'State: 4s', 11, 3) ; 0.5520610120; 6e-07
match; 4p cl coefficient ; GREP(pp/info, 'State: 4p', 11, 3) ; -0.1847719325; 4e-07
match; 3d cl coefficient ; GREP(pp/info, 'State: 3d', 11, 3) ; -29.1184260913; 3e-07

#Pseudo-Wavefunctions
match; 4s Wavefunction (small r)        ; LINE(pp/wf-4s, 417, 20) ; 1.34984642E-01; 4e-07
match; 4s Wavefunction (large r)        ; LINE(pp/wf-4s, 664, 20) ; 2.54037702E-03; 10e-09
match; 4s Wavefunction Deriv. (small r) ; LINE(pp/wf-4s, 417, 39) ; 9.50751665E-03; 9e-08
match; 4s Wavefunction Deriv. (large r) ; LINE(pp/wf-4s, 664, 39) ; -1.60970308E-03; 10e-09
match; 4p Wavefunction (small r)        ; LINE(pp/wf-4p, 417, 20) ; 8.76717419E-03; 2e-07
match; 4p Wavefunction (large r)        ; LINE(pp/wf-4p, 664, 20) ; 1.27542325E-02; 2e-07
match; 4p Wavefunction Deriv. (small r) ; LINE(pp/wf-4p, 417, 39) ; 8.64750021E-02; 4e-06
match; 4p Wavefunction Deriv. (large r) ; LINE(pp/wf-4p, 664, 39) ; -4.78717017E-03; 2e-07
match; 3d Wavefunction (small r)        ; LINE(pp/wf-3d, 417, 20) ; 1.87785146E-01; 2e-07
match; 3d Wavefunction (large r)        ; LINE(pp/wf-3d, 664, 20) ; 7.47223133E-04; 3e-09
match; 3d Wavefunction Deriv. (small r) ; LINE(pp/wf-3d, 417, 39) ; 3.53744501E+00; 4e-06
match; 3d Wavefunction Deriv. (large r) ; LINE(pp/wf-3d, 664, 39) ; -4.52908827E-04; 3e-09

#Pseudopotentials
match; s Pseudopotential (small r) ; LINE(pp/pp-s, 417, 20) ; -3.95109026E+00; 2e-06
match; s Pseudopotential (large r) ; LINE(pp/pp-s, 664, 20) ; -5.89061581E-01; 2e-08
match; p Pseudopotential (small r) ; LINE(pp/pp-p, 417, 20) ; -5.03574105E+00; 9e-07
match; p Pseudopotential (large r) ; LINE(pp/pp-p, 664, 20) ; -5.89061581E-01; 2e-08
match; d Pseudopotential (small r) ; LINE(pp/pp-d, 417, 20) ; -3.32737727E+01; 2e-05
match; d Pseudopotential (large r) ; LINE(pp/pp-d, 664, 20) ; -5.89061581E-01; 2e-08

# Self-Consistency tests
match; 4s Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 2) ; 1.0000005; 1e-12
match; 4s Slope test ; GREP(pp/info, 'Self-Consistency', 39, 2) ; 0.9999975; 10e-08
match; 4p Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 3) ; 1.0000088; 1e-12
match; 4p Slope test ; GREP(pp/info, 'Self-Consistency', 39, 3) ; 1.0000142; 1e-12
match; 3d Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 4) ; 1.0261954; 5e-07
match; 3d Slope test ; GREP(pp/info, 'Self-Consistency', 39, 4) ; 1.0156860; 3e-07


Input: 02-scalar_rel.03-tm.inp

#TM coefficients
match; 4s c0  Coefficient ; GREP(pp/info, 'State: 4s', 12, 4) ; -2.148329825E+00; 3e-07
match; 4s c2  Coefficient ; GREP(pp/info, 'State: 4s', 37, 4) ; 3.560820095E-01; 7e-07
match; 4s c4  Coefficient ; GREP(pp/info, 'State: 4s', 12, 5) ; -2.535887949E-02; 9e-08
match; 4s c6  Coefficient ; GREP(pp/info, 'State: 4s', 37, 5) ; -1.104829097E-02; 8e-09
match; 4s c8  Coefficient ; GREP(pp/info, 'State: 4s', 12, 6) ; 2.317756904E-03; 2e-09
match; 4s c10 Coefficient ; GREP(pp/info, 'State: 4s', 37, 6) ; -1.760860275E-04; 1e-10
match; 4s c12 Coefficient ; GREP(pp/info, 'State: 4s', 12, 7) ; 4.886868948E-06; 3e-12
match; 4p c0  Coefficient ; GREP(pp/info, 'State: 4p', 12, 4) ; -2.513821420E+00; 4e-06
match; 4p c2  Coefficient ; GREP(pp/info, 'State: 4p', 37, 4) ; -3.901152558E-02; 8e-08
match; 4p c4  Coefficient ; GREP(pp/info, 'State: 4p', 12, 5) ; -2.174142409E-04; 9e-10
match; 4p c6  Coefficient ; GREP(pp/info, 'State: 4p', 37, 5) ; -7.343327320E-04; 4e-09
match; 4p c8  Coefficient ; GREP(pp/info, 'State: 4p', 12, 6) ; 8.416210641E-05; 3e-10
match; 4p c10 Coefficient ; GREP(pp/info, 'State: 4p', 37, 6) ; -3.657385684E-06; 2e-11
match; 4p c12 Coefficient ; GREP(pp/info, 'State: 4p', 12, 7) ; 5.794492588E-08; 1e-12
match; 3d c0  Coefficient ; GREP(pp/info, 'State: 3d', 12, 4) ; 2.535538813E+00; 2e-07
match; 3d c2  Coefficient ; GREP(pp/info, 'State: 3d', 37, 4) ; -3.461785812E+00; 7e-07
match; 3d c4  Coefficient ; GREP(pp/info, 'State: 3d', 12, 5) ; -1.331551251E+00; 6e-07
match; 3d c6  Coefficient ; GREP(pp/info, 'State: 3d', 37, 5) ; 4.581550225E+00; 3e-06
match; 3d c8  Coefficient ; GREP(pp/info, 'State: 3d', 12, 6) ; -3.755777042E+00; 3e-06
match; 3d c10 Coefficient ; GREP(pp/info, 'State: 3d', 37, 6) ; 1.386040576E+00; 2e-06
match; 3d c12 Coefficient ; GREP(pp/info, 'State: 3d', 12, 7) ; -1.980530062E-01; 2e-07

#Pseudo-Wavefunctions
match; 4s Wavefunction (small r)        ; LINE(pp/wf-4s, 417, 20) ; 1.17105816E-01; 3e-08
match; 4s Wavefunction (large r)        ; LINE(pp/wf-4s, 664, 20) ; 2.54038372E-03; 2e-08
match; 4s Wavefunction Deriv. (small r) ; LINE(pp/wf-4s, 417, 39) ; 8.43717639E-03; 2e-08
match; 4s Wavefunction Deriv. (large r) ; LINE(pp/wf-4s, 664, 39) ; -1.60970733E-03; 2e-08
match; 4p Wavefunction (small r)        ; LINE(pp/wf-4p, 417, 20) ; 8.19908951E-03; 4e-08
match; 4p Wavefunction (large r)        ; LINE(pp/wf-4p, 664, 20) ; 1.27544784E-02; 7e-08
match; 4p Wavefunction Deriv. (small r) ; LINE(pp/wf-4p, 417, 39) ; 8.08610351E-02; 4e-07
match; 4p Wavefunction Deriv. (large r) ; LINE(pp/wf-4p, 664, 39) ; -4.78726246E-03; 4e-08
match; 3d Wavefunction (small r)        ; LINE(pp/wf-3d, 417, 20) ; 1.25036178E-01; 2e-08
match; 3d Wavefunction (large r)        ; LINE(pp/wf-3d, 664, 20) ; 7.47911116E-04; 3e-09
match; 3d Wavefunction Deriv. (small r) ; LINE(pp/wf-3d, 417, 39) ; 2.37987453E+00; 4e-07
match; 3d Wavefunction Deriv. (large r) ; LINE(pp/wf-3d, 664, 39) ; -4.53325829E-04; 3e-09

#Pseudopotentials
match; s Pseudopotential (small r) ; LINE(pp/pp-s, 417, 20) ; -3.84775069E+00; 3e-06
match; s Pseudopotential (large r) ; LINE(pp/pp-s, 664, 20) ; -5.89059066E-01; 1e-12
match; p Pseudopotential (small r) ; LINE(pp/pp-p, 417, 20) ; -4.99663674E+00; 2e-06
match; p Pseudopotential (large r) ; LINE(pp/pp-p, 664, 20) ; -5.89059066E-01; 1e-12
match; d Pseudopotential (small r) ; LINE(pp/pp-d, 417, 20) ; -2.90900011E+01; 4e-06
match; d Pseudopotential (large r) ; LINE(pp/pp-d, 664, 20) ; -5.89059066E-01; 1e-12

# Self-Consistency tests
match; 4s Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 2) ; 1.0000046; 3e-06
match; 4s Slope test ; GREP(pp/info, 'Self-Consistency', 39, 2) ; 1.0000070; 4e-06
match; 4p Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 3) ; 1.0000208; 2e-06
match; 4p Slope test ; GREP(pp/info, 'Self-Consistency', 39, 3) ; 1.0000258; 2e-06
match; 3d Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 4) ; 1.0185007; 6e-06
match; 3d Slope test ; GREP(pp/info, 'Self-Consistency', 39, 4) ; 1.0112567; 4e-06


Input: 02-scalar_rel.04-mrpp.inp

#MRPP coefficients
match; 3s c0  Coefficient ; GREP(pp/info, 'State: 3s', 12, 4) ; 2.964056618E-01; 3e-05
match; 3s c2  Coefficient ; GREP(pp/info, 'State: 3s', 37, 4) ; 7.031133868E-01; 9e-05
match; 3s c4  Coefficient ; GREP(pp/info, 'State: 3s', 12, 5) ; -9.887368696E-02; 3e-05
match; 3s c6  Coefficient ; GREP(pp/info, 'State: 3s', 37, 5) ; -2.728430549E+00; 2e-04
match; 3s c8  Coefficient ; GREP(pp/info, 'State: 3s', 12, 6) ; 3.089781422E+00; 3e-04
match; 3s c10 Coefficient ; GREP(pp/info, 'State: 3s', 37, 6) ; -1.583225046E+00; 2e-04
match; 3s c12 Coefficient ; GREP(pp/info, 'State: 3s', 12, 7) ; 4.353025556E-01; 4e-05
match; 3s c14 Coefficient ; GREP(pp/info, 'State: 3s', 37, 7) ; -6.251343552E-02; 6e-06
match; 3s c16 Coefficient ; GREP(pp/info, 'State: 3s', 12, 8) ; 3.696194689E-03; 4e-07
match; 3p c0  Coefficient ; GREP(pp/info, 'State: 3p', 12, 4) ; 1.492349239E+00; 5e-04
match; 3p c2  Coefficient ; GREP(pp/info, 'State: 3p', 37, 4) ; -1.416434817E+00; 2e-03
match; 3p c4  Coefficient ; GREP(pp/info, 'State: 3p', 12, 5) ; -2.866125129E-01; 6e-04
match; 3p c6  Coefficient ; GREP(pp/info, 'State: 3p', 37, 5) ; 2.530286845E-01; 4e-03
match; 3p c8  Coefficient ; GREP(pp/info, 'State: 3p', 12, 6) ; -1.874033747E-03; 4e-03
match; 3p c10 Coefficient ; GREP(pp/info, 'State: 3p', 37, 6) ; -4.731394189E-02; 2e-03
match; 3p c12 Coefficient ; GREP(pp/info, 'State: 3p', 12, 7) ; 1.824898853E-02; 5e-04
match; 3p c14 Coefficient ; GREP(pp/info, 'State: 3p', 37, 7) ; -2.888126669E-03; 6e-05
match; 3p c16 Coefficient ; GREP(pp/info, 'State: 3p', 12, 8) ; 1.727539440E-04; 3e-06

#Pseudo-Wavefunctions
match; 3s Wavefunction (small r)        ; LINE(pp/wf-3s, 417, 20) ; 1.35474026E+00; 4e-05
match; 3s Wavefunction (large r)        ; LINE(pp/wf-3s, 664, 20) ; 1.44007407E-10; 1e-12
match; 3s Wavefunction Deriv. (small r) ; LINE(pp/wf-3s, 417, 39) ; 1.92224063E-01; 2e-05
match; 3s Wavefunction Deriv. (large r) ; LINE(pp/wf-3s, 664, 39) ; -3.48142535E-10; 1e-12
match; 3p Wavefunction (small r)        ; LINE(pp/wf-3p, 417, 20) ; 4.44088867E-01; 3e-04
match; 3p Wavefunction (large r)        ; LINE(pp/wf-3p, 664, 20) ; 1.12556934E-08; 1e-12
match; 3p Wavefunction Deriv. (small r) ; LINE(pp/wf-3p, 417, 39) ; 4.25522023E+00; 2e-03
match; 3p Wavefunction Deriv. (large r) ; LINE(pp/wf-3p, 664, 39) ; -2.16294128E-08; 1e-12
match; 4s Wavefunction (small r)        ; LINE(pp/wf-4s, 417, 20) ; -4.38282279E-01; 2e-05
match; 4s Wavefunction (large r)        ; LINE(pp/wf-4s, 664, 20) ; 2.54038372E-03; 2e-09
match; 4s Wavefunction Deriv. (small r) ; LINE(pp/wf-4s, 417, 39) ; 1.31071764E-02; 9e-06
match; 4s Wavefunction Deriv. (large r) ; LINE(pp/wf-4s, 664, 39) ; -1.60970733E-03; 5e-10
match; 4p Wavefunction (small r)        ; LINE(pp/wf-4p, 417, 20) ; -8.22648046E-02; 5e-05
match; 4p Wavefunction (large r)        ; LINE(pp/wf-4p, 664, 20) ; 1.27544784E-02; 8e-09
match; 4p Wavefunction Deriv. (small r) ; LINE(pp/wf-4p, 417, 39) ; -7.82772590E-01; 5e-04
match; 4p Wavefunction Deriv. (large r) ; LINE(pp/wf-4p, 664, 39) ; -4.78726246E-03; 3e-09

#Pseudopotentials
match; s Pseudopotential (small r) ; LINE(pp/pp-s, 417, 20) ; -1.46332808E+01; 7e-04
match; s Pseudopotential (large r) ; LINE(pp/pp-s, 664, 20) ; -1.37447115E+00; 1e-12
match; p Pseudopotential (small r) ; LINE(pp/pp-p, 417, 20) ; -2.27809916E+01; 7e-03
match; p Pseudopotential (large r) ; LINE(pp/pp-p, 664, 20) ; -1.37447115E+00; 1e-12
match; d Pseudopotential (small r) ; LINE(pp/pp-d, 417, 20) ; -3.83614095E+01; 4e-04
match; d Pseudopotential (large r) ; LINE(pp/pp-d, 664, 20) ; -1.37447115E+00; 1e-12

# Self-Consistency tests
match; 3s Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 2) ; 1.0000014; 1e-12
match; 3s Slope test ; GREP(pp/info, 'Self-Consistency', 39, 2) ; 0.9999723; 1e-12
match; 3p Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 3) ; 0.9979278; 7e-06
match; 3p Slope test ; GREP(pp/info, 'Self-Consistency', 39, 3) ; 1.0010906; 5e-07
match; 4s Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 5) ; 0.9965385; 9e-05
match; 4s Slope test ; GREP(pp/info, 'Self-Consistency', 39, 5) ; 1.0001843; 2e-06
match; 4p Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 6) ; 1.0185005; 2e-07
match; 4p Slope test ; GREP(pp/info, 'Self-Consistency', 39, 6) ; 1.0112566; 10e-08

