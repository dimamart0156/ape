
Test     : Pseudopotentials: non-relativistic Li
Programs : ape
TestGroups : all; ps
Enabled  : Yes

Input: 01-non_rel.01-ae.inp

Input: 01-non_rel.02-ham.inp

#Coefficient
match; 2s cl coefficient ; GREP(pp/info, 'State: 2s', 11, 3) ; 0.1270412911; 8e-08
match; 2p cl coefficient ; GREP(pp/info, 'State: 2p', 11, 3) ; -0.9176124411; 6e-08

#Pseudo-Wavefunctions
match; 2s Wavefunction (small r)        ; LINE(pp/wf-2s,  98, 20) ; 1.62110855E-01; 3e-07
match; 2s Wavefunction (large r)        ; LINE(pp/wf-2s, 176, 20) ; 7.43960401E-02; 5e-08
match; 2s Wavefunction Deriv. (small r) ; LINE(pp/wf-2s,  98, 39) ; 4.39386383E-04; 1e-09
match; 2s Wavefunction Deriv. (large r) ; LINE(pp/wf-2s, 176, 39) ; -3.39767216E-02; 2e-07
match; 2p Wavefunction (small r)        ; LINE(pp/wf-2p,  98, 20) ; 2.66407803E-03; 3e-09
match; 2p Wavefunction (large r)        ; LINE(pp/wf-2p, 176, 20) ; 7.89279579E-02; 3e-08
match; 2p Wavefunction Deriv. (small r) ; LINE(pp/wf-2p,  98, 39) ; 1.80992411E-01; 2e-07
match; 2p Wavefunction Deriv. (large r) ; LINE(pp/wf-2p, 176, 39) ; -2.38712036E-02; 3e-08

#Pseudopotentials
match; s Pseudopotential (small r) ; LINE(pp/pp-s,  98, 20) ; 1.61388794E-02; 5e-07
match; s Pseudopotential (large r) ; LINE(pp/pp-s, 176, 20) ; -1.96480214E-01; 5e-08
match; p Pseudopotential (small r) ; LINE(pp/pp-p,  98, 20) ; -9.93950072E-01; 4e-07
match; p Pseudopotential (large r) ; LINE(pp/pp-p, 176, 20) ; -1.96480214E-01; 5e-08

# Self-Consistency tests
match; 2s Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 2) ; 1.0000049; 2e-07
match; 2s Slope test ; GREP(pp/info, 'Self-Consistency', 39, 2) ; 0.9999926; 2e-06
match; 2p Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 3) ; 0.9998834; 2e-07
match; 2p Slope test ; GREP(pp/info, 'Self-Consistency', 39, 3) ; 0.9998173; 1e-12

#KB  projectors
match; Vanderbilt function v0 ; GREP(kb/info, "Vanderbilt function", 20, 2) ; -0.491383; 1e-12
match; Vanderbilt function v1 ; GREP(kb/info, "Vanderbilt function", 32, 2) ; -0.057906; 1e-12
match; Vanderbilt function v2 ; GREP(kb/info, "Vanderbilt function", 44, 2) ; 0.001118; 1e-12
match; Vanderbilt function v3 ; GREP(kb/info, "Vanderbilt function", 56, 2) ; -0.000010; 1e-12
match; 2s KB Energy ; GREP(kb/info, "KB Energy", 13, 1) ; 1.0840; 1e-12
match; 2p KB Energy ; GREP(kb/info, "KB Energy", 13, 2) ; -0.4609; 1e-12
match; 2s KB Cosine ; GREP(kb/info, "KB Energy", 28, 1) ; 0.0761; 1e-12
match; 2p KB Cosine ; GREP(kb/info, "KB Energy", 28, 2) ; -0.2732; 1e-12
match; Local component ; LINE(kb/kb-local, 98, 20) ; -4.91376898E-01; 1e-12
match; s component     ; LINE(kb/kb-s,     98, 20) ; 9.97505403E-01; 4e-06
match; p component     ; LINE(kb/kb-p,     98, 20) ; -1.06336095E-02; 2e-08

#KB tests
match; 2s Local potential E0 ; GREP(kb/info, "Ghost state analysis", 35, 3) ; -0.1215; 1e-12
match; 2s Local potential E1 ; GREP(kb/info, "Ghost state analysis", 51, 3) ; -0.0050; 1e-12
match; 2p Local potential E0 ; GREP(kb/info, "Ghost state analysis", 35, 7) ; -0.0248; 1e-12
match; 2p Local potential E1 ; GREP(kb/info, "Ghost state analysis", 51, 7) ; 0.0000; 1e-12
match; Local localization radius ; GREP(kb/info, "Localization", 11, 1) ; 3.77; 1e-12
match; s localization radius     ; GREP(kb/info, "Localization", 11, 2) ; 4.06; 1e-12
match; p localization radius     ; GREP(kb/info, "Localization", 11, 3) ; 3.77; 1e-12



Input: 01-non_rel.03-tm.inp

#TM coefficients
match; 2s c0  Coefficient ; GREP(pp/info, 'State: 2s', 12, 4) ; -1.808723564E+00; 3e-07
match; 2s c2  Coefficient ; GREP(pp/info, 'State: 2s', 37, 4) ; 8.939589361E-02; 2e-07
match; 2s c4  Coefficient ; GREP(pp/info, 'State: 2s', 12, 5) ; -1.598324978E-03; 6e-09
match; 2s c6  Coefficient ; GREP(pp/info, 'State: 2s', 37, 5) ; -3.559105858E-03; 5e-09
match; 2s c8  Coefficient ; GREP(pp/info, 'State: 2s', 12, 6) ; 5.110757192E-04; 8e-10
match; 2s c10 Coefficient ; GREP(pp/info, 'State: 2s', 37, 6) ; -2.910108951E-05; 5e-11
match; 2s c12 Coefficient ; GREP(pp/info, 'State: 2s', 12, 7) ; 6.159471230E-07; 2e-12
match; 2p c0  Coefficient ; GREP(pp/info, 'State: 2p', 12, 4) ; -1.864359748E+00; 4e-07
match; 2p c2  Coefficient ; GREP(pp/info, 'State: 2p', 37, 4) ; -1.625116719E-01; 8e-08
match; 2p c4  Coefficient ; GREP(pp/info, 'State: 2p', 12, 5) ; -3.772863384E-03; 4e-09
match; 2p c6  Coefficient ; GREP(pp/info, 'State: 2p', 37, 5) ; 1.560281934E-03; 3e-09
match; 2p c8  Coefficient ; GREP(pp/info, 'State: 2p', 12, 6) ; -1.259324535E-04; 3e-10
match; 2p c10 Coefficient ; GREP(pp/info, 'State: 2p', 37, 6) ; 4.490661756E-06; 9e-12
match; 2p c12 Coefficient ; GREP(pp/info, 'State: 2p', 12, 7) ; -6.155177598E-08; 1e-12

#Pseudo-Wavefunctions
match; 2s Wavefunction (small r)        ; LINE(pp/wf-2s,  98, 20) ; 1.63866338E-01; 5e-08
match; 2s Wavefunction (large r)        ; LINE(pp/wf-2s, 176, 20) ; 7.43968068E-02; 2e-08
match; 2s Wavefunction Deriv. (small r) ; LINE(pp/wf-2s,  98, 39) ; 4.31266665E-04; 6e-10
match; 2s Wavefunction Deriv. (large r) ; LINE(pp/wf-2s, 176, 39) ; -3.39791607E-02; 3e-08
match; 2p Wavefunction (small r)        ; LINE(pp/wf-2p,  98, 20) ; 2.28147387E-03; 7e-10
match; 2p Wavefunction (large r)        ; LINE(pp/wf-2p, 176, 20) ; 7.89278363E-02; 10e-09
match; 2p Wavefunction Deriv. (small r) ; LINE(pp/wf-2p,  98, 39) ; 1.54979041E-01; 5e-08
match; 2p Wavefunction Deriv. (large r) ; LINE(pp/wf-2p, 176, 39) ; -2.38711668E-02; 4e-08

#Pseudopotentials
match; s Pseudopotential (small r) ; LINE(pp/pp-s,  98, 20) ; 8.99477193E-03; 4e-07
match; s Pseudopotential (large r) ; LINE(pp/pp-s, 176, 20) ; -1.96479556E-01; 1e-12
match; p Pseudopotential (small r) ; LINE(pp/pp-p,  98, 20) ; -1.00755265E+00; 5e-07
match; p Pseudopotential (large r) ; LINE(pp/pp-p, 176, 20) ; -1.96479556E-01; 1e-12

# Self-Consistency tests
match; 2s Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 2) ; 1.0000846; 6e-07
match; 2s Slope test ; GREP(pp/info, 'Self-Consistency', 39, 2) ; 1.0002735; 9e-07
match; 2p Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 3) ; 1.0001030; 2e-06
match; 2p Slope test ; GREP(pp/info, 'Self-Consistency', 39, 3) ; 0.9998276; 2e-06

#KB  projectors
match; 2s KB Energy ; GREP(kb/info, "KB Energy", 13, 1) ; 0.6676; 1e-12
match; 2s KB Cosine ; GREP(kb/info, "KB Energy", 28, 1) ; 0.2916; 1e-12
match; Local component ; LINE(kb/kb-local, 98, 20) ; -1.00755265E+00; 5e-07
match; s component     ; LINE(kb/kb-s,     98, 20) ; 8.55690856E-01; 10e-08

#KB tests
match; 2s Local potential E0 ; GREP(kb/info, "Ghost state analysis", 35, 3) ; -0.2937; 1e-12
match; 2s Local potential E1 ; GREP(kb/info, "Ghost state analysis", 51, 3) ; -0.0235; 1e-12
match; Local localization radius ; GREP(kb/info, "Localization", 11, 1) ; 3.50; 1e-12
match; s localization radius     ; GREP(kb/info, "Localization", 11, 2) ; 3.50; 1e-12



Input: 01-non_rel.04-mrpp.inp

#MRPP coefficients
match; 1s c0  Coefficient ; GREP(pp/info, 'State: 1s', 12, 4) ; 1.635236867E+00; 2e-04
match; 1s c2  Coefficient ; GREP(pp/info, 'State: 1s', 37, 4) ; -2.994929913E+00; 8e-04
match; 1s c4  Coefficient ; GREP(pp/info, 'State: 1s', 12, 5) ; -1.793921040E+00; 9e-04
match; 1s c6  Coefficient ; GREP(pp/info, 'State: 1s', 37, 5) ; 8.339824222E+00; 7e-03
match; 1s c8  Coefficient ; GREP(pp/info, 'State: 1s', 12, 6) ; -1.028218577E+01; 2e-02
match; 1s c10 Coefficient ; GREP(pp/info, 'State: 1s', 37, 6) ; 6.538033751E+00; 8e-03
match; 1s c12 Coefficient ; GREP(pp/info, 'State: 1s', 12, 7) ; -2.325158041E+00; 4e-03
match; 1s c14 Coefficient ; GREP(pp/info, 'State: 1s', 37, 7) ; 4.400481978E-01; 7e-04
match; 1s c16 Coefficient ; GREP(pp/info, 'State: 1s', 12, 8) ; -3.463311973E-02; 6e-05

#Pseudo-Wavefunctions
match; 1s Wavefunction (small r)        ; LINE(pp/wf-1s,  98, 20) ; 5.12734424E+00; 7e-04
match; 1s Wavefunction (large r)        ; LINE(pp/wf-1s, 176, 20) ; 8.09428066E-05; 4e-10
match; 1s Wavefunction Deriv. (small r) ; LINE(pp/wf-1s,  98, 39) ; -4.52203196E-01; 2e-04
match; 1s Wavefunction Deriv. (large r) ; LINE(pp/wf-1s, 176, 39) ; -1.68554598E-04; 7e-10
match; 2s Wavefunction (small r)        ; LINE(pp/wf-2s,  98, 20) ; -8.79786973E-01; 2e-04
match; 2s Wavefunction (large r)        ; LINE(pp/wf-2s, 176, 20) ; 7.43968068E-02; 2e-08
match; 2s Wavefunction Deriv. (small r) ; LINE(pp/wf-2s,  98, 39) ; 9.29053398E-02; 4e-05
match; 2s Wavefunction Deriv. (large r) ; LINE(pp/wf-2s, 176, 39) ; -3.39791607E-02; 3e-08

#Pseudopotentials
match; s Pseudopotential (small r) ; LINE(pp/pp-s,  98, 20) ; -1.42732761E+01; 3e-03
match; s Pseudopotential (large r) ; LINE(pp/pp-s, 176, 20) ; -5.89436925E-01; 8e-09
match; p Pseudopotential (small r) ; LINE(pp/pp-p,  98, 20) ; -4.26423835E+00; 2e-04
match; p Pseudopotential (large r) ; LINE(pp/pp-p, 176, 20) ; -5.89436925E-01; 8e-09

# Self-Consistency tests
match; 1s Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 2) ; 1.0000062; 1e-12
match; 1s Slope test ; GREP(pp/info, 'Self-Consistency', 39, 2) ; 0.9994655; 2e-06
match; 2s Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 3) ; 0.9789805; 8e-05
match; 2s Slope test ; GREP(pp/info, 'Self-Consistency', 39, 3) ; 1.0062765; 1e-06
match; 2p Norm test  ; GREP(pp/info, 'Self-Consistency', 27, 4) ; 1.0001030; 2e-06
match; 2p Slope test ; GREP(pp/info, 'Self-Consistency', 39, 4) ; 0.9998276; 2e-06

#KB  projectors
match; 2s KB Energy    ; GREP(kb/info, "KB Energy", 13, 1) ; -8.5823; 2e-03
match; 2s KB Cosine    ; GREP(kb/info, "KB Energy", 28, 1) ; -0.7968; 2e-04
match; Local component ; LINE(kb/kb-local, 98, 20) ; -1.98941279E+00; 1e-12
match; s component     ; LINE(kb/kb-s,     98, 20) ; -9.20985436E+00; 2e-03

#KB tests
match; 2s Local potential E0     ; GREP(kb/info, "Ghost state analysis", 35, 3) ; -0.1036; 1e-12
match; 2s Local potential E1     ; GREP(kb/info, "Ghost state analysis", 51, 3) ; -0.0022; 1e-12
match; Local localization radius ; GREP(kb/info, "Localization", 11, 1) ; 3.01; 1e-12
match; s localization radius     ; GREP(kb/info, "Localization", 11, 2) ; 1.92; 1e-12
